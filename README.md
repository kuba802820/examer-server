# README - Server for Examination Platform 

## Introduction
This project is a server for the examination platform.
## Features
The API offers the following features:
- Login and registration: Users can register and log in to the system.
- Creating questions: Teachers can create questions for exams.
- Exam management: Teachers can create, edit, and delete exams.
- Question management: Teachers can manage questions within a given exam.
## Technologies Used
- Typescript
- GraphQL
- Express
- Prisma
- Apollo
- Nexus
- PostgreSQL
