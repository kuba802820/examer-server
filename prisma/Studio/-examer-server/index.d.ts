import {
  DMMF,
  DMMFClass,
  Engine,
  PrismaClientKnownRequestError,
  PrismaClientUnknownRequestError,
  PrismaClientRustPanicError,
  PrismaClientInitializationError,
  PrismaClientValidationError,
  sqltag as sql,
  empty,
  join,
  raw,
} from './runtime';

export { PrismaClientKnownRequestError }
export { PrismaClientUnknownRequestError }
export { PrismaClientRustPanicError }
export { PrismaClientInitializationError }
export { PrismaClientValidationError }

/**
 * Re-export of sql-template-tag
 */
export { sql, empty, join, raw }

/**
 * Prisma Client JS version: 2.8.1
 * Query Engine version: 439da16b2f8314c6faca7d2dad2cdcf0732e8a9c
 */
export declare type PrismaVersion = {
  client: string
}

export declare const prismaVersion: PrismaVersion 

/**
 * Utility Types
 */

/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches a JSON object.
 * This type can be useful to enforce some input to be JSON-compatible or as a super-type to be extended from. 
 */
export declare type JsonObject = {[Key in string]?: JsonValue}
 
/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches a JSON array.
 */
export declare interface JsonArray extends Array<JsonValue> {}
 
/**
 * From https://github.com/sindresorhus/type-fest/
 * Matches any valid JSON value.
 */
export declare type JsonValue = string | number | boolean | null | JsonObject | JsonArray

/**
 * Same as JsonObject, but allows undefined
 */
export declare type InputJsonObject = {[Key in string]?: JsonValue}
 
export declare interface InputJsonArray extends Array<JsonValue> {}
 
export declare type InputJsonValue = undefined |  string | number | boolean | null | InputJsonObject | InputJsonArray

declare type SelectAndInclude = {
  select: any
  include: any
}

declare type HasSelect = {
  select: any
}

declare type HasInclude = {
  include: any
}

declare type CheckSelect<T, S, U> = T extends SelectAndInclude
  ? 'Please either choose `select` or `include`'
  : T extends HasSelect
  ? U
  : T extends HasInclude
  ? U
  : S

/**
 * Get the type of the value, that the Promise holds.
 */
export declare type PromiseType<T extends PromiseLike<any>> = T extends PromiseLike<infer U> ? U : T;

/**
 * Get the return type of a function which returns a Promise.
 */
export declare type PromiseReturnType<T extends (...args: any) => Promise<any>> = PromiseType<ReturnType<T>>


export declare type Enumerable<T> = T | Array<T>;

export type RequiredKeys<T> = {
  [K in keyof T]-?: {} extends Pick<T, K> ? never : K
}[keyof T]

export declare type TruthyKeys<T> = {
  [key in keyof T]: T[key] extends false | undefined | null ? never : key
}[keyof T]

export declare type TrueKeys<T> = TruthyKeys<Pick<T, RequiredKeys<T>>>

/**
 * Subset
 * @desc From `T` pick properties that exist in `U`. Simple version of Intersection
 */
export declare type Subset<T, U> = {
  [key in keyof T]: key extends keyof U ? T[key] : never;
};
declare class PrismaClientFetcher {
  private readonly prisma;
  private readonly debug;
  private readonly hooks?;
  constructor(prisma: PrismaClient<any, any>, debug?: boolean, hooks?: Hooks | undefined);
  request<T>(document: any, dataPath?: string[], rootField?: string, typeName?: string, isList?: boolean, callsite?: string): Promise<T>;
  sanitizeMessage(message: string): string;
  protected unpack(document: any, data: any, path: string[], rootField?: string, isList?: boolean): any;
}


/**
 * Client
**/

export declare type Datasource = {
  url?: string
}

export type Datasources = {
  db?: Datasource
}

export type ErrorFormat = 'pretty' | 'colorless' | 'minimal'

export interface PrismaClientOptions {
  /**
   * Overwrites the datasource url from your prisma.schema file
   */
  datasources?: Datasources

  /**
   * @default "colorless"
   */
  errorFormat?: ErrorFormat

  /**
   * @example
   * ```
   * // Defaults to stdout
   * log: ['query', 'info', 'warn', 'error']
   * 
   * // Emit as events
   * log: [
   *  { emit: 'stdout', level: 'query' },
   *  { emit: 'stdout', level: 'info' },
   *  { emit: 'stdout', level: 'warn' }
   *  { emit: 'stdout', level: 'error' }
   * ]
   * ```
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/logging#the-log-option).
   */
  log?: Array<LogLevel | LogDefinition>
}

export type Hooks = {
  beforeRequest?: (options: {query: string, path: string[], rootField?: string, typeName?: string, document: any}) => any
}

/* Types for Logging */
export type LogLevel = 'info' | 'query' | 'warn' | 'error'
export type LogDefinition = {
  level: LogLevel
  emit: 'stdout' | 'event'
}

export type GetLogType<T extends LogLevel | LogDefinition> = T extends LogDefinition ? T['emit'] extends 'event' ? T['level'] : never : never
export type GetEvents<T extends any> = T extends Array<LogLevel | LogDefinition> ?
  GetLogType<T[0]> | GetLogType<T[1]> | GetLogType<T[2]> | GetLogType<T[3]>
  : never

export type QueryEvent = {
  timestamp: Date
  query: string
  params: string
  duration: number
  target: string
}

export type LogEvent = {
  timestamp: Date
  message: string
  target: string
}
/* End Types for Logging */


export type PrismaAction =
  | 'findOne'
  | 'findMany'
  | 'findFirst'
  | 'create'
  | 'update'
  | 'updateMany'
  | 'upsert'
  | 'delete'
  | 'deleteMany'
  | 'executeRaw'
  | 'queryRaw'
  | 'aggregate'

/**
 * These options are being passed in to the middleware as "params"
 */
export type MiddlewareParams = {
  model?: string
  action: PrismaAction
  args: any
  dataPath: string[]
  runInTransaction: boolean
}

/**
 * The `T` type makes sure, that the `return proceed` is not forgotten in the middleware implementation
 */
export type Middleware<T = any> = (
  params: MiddlewareParams,
  next: (params: MiddlewareParams) => Promise<T>,
) => Promise<T>

// tested in getLogLevel.test.ts
export declare function getLogLevel(log: Array<LogLevel | LogDefinition>): LogLevel | undefined;

/**
 * ##  Prisma Client ʲˢ
 * 
 * Type-safe database client for TypeScript & Node.js (ORM replacement)
 * @example
 * ```
 * const prisma = new PrismaClient()
 * // Fetch zero or more Answers
 * const answers = await prisma.answers.findMany()
 * ```
 *
 * 
 * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
 */
export declare class PrismaClient<
  T extends PrismaClientOptions = PrismaClientOptions,
  U = 'log' extends keyof T ? T['log'] extends Array<LogLevel | LogDefinition> ? GetEvents<T['log']> : never : never
> {
  /**
   * @private
   */
  private fetcher;
  /**
   * @private
   */
  private readonly dmmf;
  /**
   * @private
   */
  private connectionPromise?;
  /**
   * @private
   */
  private disconnectionPromise?;
  /**
   * @private
   */
  private readonly engineConfig;
  /**
   * @private
   */
  private readonly measurePerformance;
  /**
   * @private
   */
  private engine: Engine;
  /**
   * @private
   */
  private errorFormat: ErrorFormat;

  /**
   * ##  Prisma Client ʲˢ
   * 
   * Type-safe database client for TypeScript & Node.js (ORM replacement)
   * @example
   * ```
   * const prisma = new PrismaClient()
   * // Fetch zero or more Answers
   * const answers = await prisma.answers.findMany()
   * ```
   *
   * 
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
   */
  constructor(optionsArg?: T);
  $on<V extends U>(eventType: V, callback: (event: V extends 'query' ? QueryEvent : LogEvent) => void): void;
  /**
   * @deprecated renamed to `$on`
   */
  on<V extends U>(eventType: V, callback: (event: V extends 'query' ? QueryEvent : LogEvent) => void): void;
  /**
   * Connect with the database
   */
  $connect(): Promise<void>;
  /**
   * @deprecated renamed to `$connect`
   */
  connect(): Promise<void>;

  /**
   * Disconnect from the database
   */
  $disconnect(): Promise<any>;
  /**
   * @deprecated renamed to `$disconnect`
   */
  disconnect(): Promise<any>;

  /**
   * Add a middleware
   */
  $use(cb: Middleware): void

  /**
   * Executes a raw query and returns the number of affected rows
   * @example
   * ```
   * // With parameters use prisma.executeRaw``, values will be escaped automatically
   * const result = await prisma.executeRaw`UPDATE User SET cool = ${true} WHERE id = ${1};`
   * // Or
   * const result = await prisma.executeRaw('UPDATE User SET cool = $1 WHERE id = $2 ;', true, 1)
  * ```
  * 
  * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
  */
  $executeRaw<T = any>(query: string | TemplateStringsArray, ...values: any[]): Promise<number>;

  /**
   * @deprecated renamed to `$executeRaw`
   */
  executeRaw<T = any>(query: string | TemplateStringsArray, ...values: any[]): Promise<number>;

  /**
   * Performs a raw query and returns the SELECT data
   * @example
   * ```
   * // With parameters use prisma.queryRaw``, values will be escaped automatically
   * const result = await prisma.queryRaw`SELECT * FROM User WHERE id = ${1} OR email = ${'ema.il'};`
   * // Or
   * const result = await prisma.queryRaw('SELECT * FROM User WHERE id = $1 OR email = $2;', 1, 'ema.il')
  * ```
  * 
  * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
  */
  $queryRaw<T = any>(query: string | TemplateStringsArray, ...values: any[]): Promise<T>;
 
  /**
   * @deprecated renamed to `$queryRaw`
   */
  queryRaw<T = any>(query: string | TemplateStringsArray, ...values: any[]): Promise<T>;

  /**
   * Execute queries in a transaction
   * @example
   * ```
   * const [george, bob, alice] = await prisma.transaction([
   *   prisma.user.create({ data: { name: 'George' } }),
   *   prisma.user.create({ data: { name: 'Bob' } }),
   *   prisma.user.create({ data: { name: 'Alice' } }),
   * ])
   * ```
   */
  $transaction: PromiseConstructor['all']
  /**
   * @deprecated renamed to `$transaction`
   */
  transaction: PromiseConstructor['all']

  /**
   * `prisma.answers`: Exposes CRUD operations for the **Answers** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Answers
    * const answers = await prisma.answers.findMany()
    * ```
    */
  get answers(): AnswersDelegate;

  /**
   * `prisma.exams`: Exposes CRUD operations for the **Exams** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Exams
    * const exams = await prisma.exams.findMany()
    * ```
    */
  get exams(): ExamsDelegate;

  /**
   * `prisma.questions`: Exposes CRUD operations for the **Questions** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Questions
    * const questions = await prisma.questions.findMany()
    * ```
    */
  get questions(): QuestionsDelegate;

  /**
   * `prisma.user`: Exposes CRUD operations for the **User** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Users
    * const users = await prisma.user.findMany()
    * ```
    */
  get user(): UserDelegate;
}



/**
 * Enums
 */

// Based on
// https://github.com/microsoft/TypeScript/issues/3192#issuecomment-261720275

export declare const AnswersDistinctFieldEnum: {
  answer: 'answer',
  answeredAt: 'answeredAt',
  examerNote: 'examerNote',
  examId: 'examId',
  examinatedUserId: 'examinatedUserId',
  getScored: 'getScored',
  id: 'id',
  isCorrectAnswer: 'isCorrectAnswer',
  isRated: 'isRated',
  question: 'question',
  questionId: 'questionId'
};

export declare type AnswersDistinctFieldEnum = (typeof AnswersDistinctFieldEnum)[keyof typeof AnswersDistinctFieldEnum]


export declare const ExamsDistinctFieldEnum: {
  activeCode: 'activeCode',
  createAt: 'createAt',
  endDate: 'endDate',
  examDetails: 'examDetails',
  examerNick: 'examerNick',
  examinatedUserId: 'examinatedUserId',
  examSubject: 'examSubject',
  genQuestionsId: 'genQuestionsId',
  id: 'id',
  isActive: 'isActive',
  isPassed: 'isPassed',
  minScoreToPass: 'minScoreToPass',
  score: 'score',
  startDate: 'startDate'
};

export declare type ExamsDistinctFieldEnum = (typeof ExamsDistinctFieldEnum)[keyof typeof ExamsDistinctFieldEnum]


export declare const QuestionsDistinctFieldEnum: {
  content: 'content',
  correctAnswer: 'correctAnswer',
  id: 'id',
  image: 'image',
  odpA: 'odpA',
  odpB: 'odpB',
  odpC: 'odpC',
  odpD: 'odpD',
  score: 'score',
  type: 'type'
};

export declare type QuestionsDistinctFieldEnum = (typeof QuestionsDistinctFieldEnum)[keyof typeof QuestionsDistinctFieldEnum]


export declare const UserDistinctFieldEnum: {
  createAt: 'createAt',
  email: 'email',
  id: 'id',
  isActive: 'isActive',
  isOnExam: 'isOnExam',
  nick: 'nick',
  password: 'password',
  role: 'role'
};

export declare type UserDistinctFieldEnum = (typeof UserDistinctFieldEnum)[keyof typeof UserDistinctFieldEnum]


export declare const SortOrder: {
  asc: 'asc',
  desc: 'desc'
};

export declare type SortOrder = (typeof SortOrder)[keyof typeof SortOrder]


export declare const QueryMode: {
  default: 'default',
  insensitive: 'insensitive'
};

export declare type QueryMode = (typeof QueryMode)[keyof typeof QueryMode]



/**
 * Model Answers
 */

export type Answers = {
  answer: string
  answeredAt: Date
  examerNote: string | null
  examId: number
  examinatedUserId: number | null
  getScored: number | null
  id: number
  isCorrectAnswer: boolean | null
  isRated: boolean | null
  question: string
  questionId: number
}


export type AggregateAnswers = {
  count: number
  avg: AnswersAvgAggregateOutputType | null
  sum: AnswersSumAggregateOutputType | null
  min: AnswersMinAggregateOutputType | null
  max: AnswersMaxAggregateOutputType | null
}

export type AnswersAvgAggregateOutputType = {
  examId: number
  examinatedUserId: number | null
  getScored: number | null
  id: number
  questionId: number
}

export type AnswersSumAggregateOutputType = {
  examId: number
  examinatedUserId: number | null
  getScored: number | null
  id: number
  questionId: number
}

export type AnswersMinAggregateOutputType = {
  examId: number
  examinatedUserId: number | null
  getScored: number | null
  id: number
  questionId: number
}

export type AnswersMaxAggregateOutputType = {
  examId: number
  examinatedUserId: number | null
  getScored: number | null
  id: number
  questionId: number
}


export type AnswersAvgAggregateInputType = {
  examId?: true
  examinatedUserId?: true
  getScored?: true
  id?: true
  questionId?: true
}

export type AnswersSumAggregateInputType = {
  examId?: true
  examinatedUserId?: true
  getScored?: true
  id?: true
  questionId?: true
}

export type AnswersMinAggregateInputType = {
  examId?: true
  examinatedUserId?: true
  getScored?: true
  id?: true
  questionId?: true
}

export type AnswersMaxAggregateInputType = {
  examId?: true
  examinatedUserId?: true
  getScored?: true
  id?: true
  questionId?: true
}

export type AggregateAnswersArgs = {
  where?: AnswersWhereInput
  orderBy?: Enumerable<AnswersOrderByInput> | AnswersOrderByInput
  cursor?: AnswersWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<AnswersDistinctFieldEnum>
  count?: true
  avg?: AnswersAvgAggregateInputType
  sum?: AnswersSumAggregateInputType
  min?: AnswersMinAggregateInputType
  max?: AnswersMaxAggregateInputType
}

export type GetAnswersAggregateType<T extends AggregateAnswersArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetAnswersAggregateScalarType<T[P]>
}

export type GetAnswersAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof AnswersAvgAggregateOutputType ? AnswersAvgAggregateOutputType[P] : never
}
    
    

export type AnswersSelect = {
  answer?: boolean
  answeredAt?: boolean
  examerNote?: boolean
  examId?: boolean
  examinatedUserId?: boolean
  getScored?: boolean
  id?: boolean
  isCorrectAnswer?: boolean
  isRated?: boolean
  question?: boolean
  questionId?: boolean
  Exams?: boolean | ExamsArgs
  User?: boolean | UserArgs
  Questions?: boolean | QuestionsArgs
}

export type AnswersInclude = {
  Exams?: boolean | ExamsArgs
  User?: boolean | UserArgs
  Questions?: boolean | QuestionsArgs
}

export type AnswersGetPayload<
  S extends boolean | null | undefined | AnswersArgs,
  U = keyof S
> = S extends true
  ? Answers
  : S extends undefined
  ? never
  : S extends AnswersArgs | FindManyAnswersArgs
  ? 'include' extends U
    ? Answers  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Exams'
      ? ExamsGetPayload<S['include'][P]> :
      P extends 'User'
      ? UserGetPayload<S['include'][P]> | null :
      P extends 'Questions'
      ? QuestionsGetPayload<S['include'][P]> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Answers ? Answers[P]
: 
      P extends 'Exams'
      ? ExamsGetPayload<S['select'][P]> :
      P extends 'User'
      ? UserGetPayload<S['select'][P]> | null :
      P extends 'Questions'
      ? QuestionsGetPayload<S['select'][P]> : never
    }
  : Answers
: Answers


export interface AnswersDelegate {
  /**
   * Find zero or one Answers that matches the filter.
   * @param {FindOneAnswersArgs} args - Arguments to find a Answers
   * @example
   * // Get one Answers
   * const answers = await prisma.answers.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneAnswersArgs>(
    args: Subset<T, FindOneAnswersArgs>
  ): CheckSelect<T, Prisma__AnswersClient<Answers | null>, Prisma__AnswersClient<AnswersGetPayload<T> | null>>
  /**
   * Find the first Answers that matches the filter.
   * @param {FindFirstAnswersArgs} args - Arguments to find a Answers
   * @example
   * // Get one Answers
   * const answers = await prisma.answers.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstAnswersArgs>(
    args?: Subset<T, FindFirstAnswersArgs>
  ): CheckSelect<T, Prisma__AnswersClient<Answers | null>, Prisma__AnswersClient<AnswersGetPayload<T> | null>>
  /**
   * Find zero or more Answers that matches the filter.
   * @param {FindManyAnswersArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Answers
   * const answers = await prisma.answers.findMany()
   * 
   * // Get first 10 Answers
   * const answers = await prisma.answers.findMany({ take: 10 })
   * 
   * // Only select the `answer`
   * const answersWithAnswerOnly = await prisma.answers.findMany({ select: { answer: true } })
   * 
  **/
  findMany<T extends FindManyAnswersArgs>(
    args?: Subset<T, FindManyAnswersArgs>
  ): CheckSelect<T, Promise<Array<Answers>>, Promise<Array<AnswersGetPayload<T>>>>
  /**
   * Create a Answers.
   * @param {AnswersCreateArgs} args - Arguments to create a Answers.
   * @example
   * // Create one Answers
   * const Answers = await prisma.answers.create({
   *   data: {
   *     // ... data to create a Answers
   *   }
   * })
   * 
  **/
  create<T extends AnswersCreateArgs>(
    args: Subset<T, AnswersCreateArgs>
  ): CheckSelect<T, Prisma__AnswersClient<Answers>, Prisma__AnswersClient<AnswersGetPayload<T>>>
  /**
   * Delete a Answers.
   * @param {AnswersDeleteArgs} args - Arguments to delete one Answers.
   * @example
   * // Delete one Answers
   * const Answers = await prisma.answers.delete({
   *   where: {
   *     // ... filter to delete one Answers
   *   }
   * })
   * 
  **/
  delete<T extends AnswersDeleteArgs>(
    args: Subset<T, AnswersDeleteArgs>
  ): CheckSelect<T, Prisma__AnswersClient<Answers>, Prisma__AnswersClient<AnswersGetPayload<T>>>
  /**
   * Update one Answers.
   * @param {AnswersUpdateArgs} args - Arguments to update one Answers.
   * @example
   * // Update one Answers
   * const answers = await prisma.answers.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends AnswersUpdateArgs>(
    args: Subset<T, AnswersUpdateArgs>
  ): CheckSelect<T, Prisma__AnswersClient<Answers>, Prisma__AnswersClient<AnswersGetPayload<T>>>
  /**
   * Delete zero or more Answers.
   * @param {AnswersDeleteManyArgs} args - Arguments to filter Answers to delete.
   * @example
   * // Delete a few Answers
   * const { count } = await prisma.answers.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends AnswersDeleteManyArgs>(
    args: Subset<T, AnswersDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Answers.
   * @param {AnswersUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Answers
   * const answers = await prisma.answers.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends AnswersUpdateManyArgs>(
    args: Subset<T, AnswersUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Answers.
   * @param {AnswersUpsertArgs} args - Arguments to update or create a Answers.
   * @example
   * // Update or create a Answers
   * const answers = await prisma.answers.upsert({
   *   create: {
   *     // ... data to create a Answers
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Answers we want to update
   *   }
   * })
  **/
  upsert<T extends AnswersUpsertArgs>(
    args: Subset<T, AnswersUpsertArgs>
  ): CheckSelect<T, Prisma__AnswersClient<Answers>, Prisma__AnswersClient<AnswersGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyAnswersArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateAnswersArgs>(args: Subset<T, AggregateAnswersArgs>): Promise<GetAnswersAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Answers.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__AnswersClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Exams<T extends ExamsArgs = {}>(args?: Subset<T, ExamsArgs>): CheckSelect<T, Prisma__ExamsClient<Exams | null>, Prisma__ExamsClient<ExamsGetPayload<T> | null>>;

  User<T extends UserArgs = {}>(args?: Subset<T, UserArgs>): CheckSelect<T, Prisma__UserClient<User | null>, Prisma__UserClient<UserGetPayload<T> | null>>;

  Questions<T extends QuestionsArgs = {}>(args?: Subset<T, QuestionsArgs>): CheckSelect<T, Prisma__QuestionsClient<Questions | null>, Prisma__QuestionsClient<QuestionsGetPayload<T> | null>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Answers findOne
 */
export type FindOneAnswersArgs = {
  /**
   * Select specific fields to fetch from the Answers
  **/
  select?: AnswersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AnswersInclude | null
  /**
   * Filter, which Answers to fetch.
  **/
  where: AnswersWhereUniqueInput
}


/**
 * Answers findFirst
 */
export type FindFirstAnswersArgs = {
  /**
   * Select specific fields to fetch from the Answers
  **/
  select?: AnswersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AnswersInclude | null
  /**
   * Filter, which Answers to fetch.
  **/
  where?: AnswersWhereInput
  orderBy?: Enumerable<AnswersOrderByInput> | AnswersOrderByInput
  cursor?: AnswersWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<AnswersDistinctFieldEnum>
}


/**
 * Answers findMany
 */
export type FindManyAnswersArgs = {
  /**
   * Select specific fields to fetch from the Answers
  **/
  select?: AnswersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AnswersInclude | null
  /**
   * Filter, which Answers to fetch.
  **/
  where?: AnswersWhereInput
  /**
   * Determine the order of the Answers to fetch.
  **/
  orderBy?: Enumerable<AnswersOrderByInput> | AnswersOrderByInput
  /**
   * Sets the position for listing Answers.
  **/
  cursor?: AnswersWhereUniqueInput
  /**
   * The number of Answers to fetch. If negative number, it will take Answers before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Answers.
  **/
  skip?: number
  distinct?: Enumerable<AnswersDistinctFieldEnum>
}


/**
 * Answers create
 */
export type AnswersCreateArgs = {
  /**
   * Select specific fields to fetch from the Answers
  **/
  select?: AnswersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AnswersInclude | null
  /**
   * The data needed to create a Answers.
  **/
  data: AnswersCreateInput
}


/**
 * Answers update
 */
export type AnswersUpdateArgs = {
  /**
   * Select specific fields to fetch from the Answers
  **/
  select?: AnswersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AnswersInclude | null
  /**
   * The data needed to update a Answers.
  **/
  data: AnswersUpdateInput
  /**
   * Choose, which Answers to update.
  **/
  where: AnswersWhereUniqueInput
}


/**
 * Answers updateMany
 */
export type AnswersUpdateManyArgs = {
  data: AnswersUpdateManyMutationInput
  where?: AnswersWhereInput
}


/**
 * Answers upsert
 */
export type AnswersUpsertArgs = {
  /**
   * Select specific fields to fetch from the Answers
  **/
  select?: AnswersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AnswersInclude | null
  /**
   * The filter to search for the Answers to update in case it exists.
  **/
  where: AnswersWhereUniqueInput
  /**
   * In case the Answers found by the `where` argument doesn't exist, create a new Answers with this data.
  **/
  create: AnswersCreateInput
  /**
   * In case the Answers was found with the provided `where` argument, update it with this data.
  **/
  update: AnswersUpdateInput
}


/**
 * Answers delete
 */
export type AnswersDeleteArgs = {
  /**
   * Select specific fields to fetch from the Answers
  **/
  select?: AnswersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AnswersInclude | null
  /**
   * Filter which Answers to delete.
  **/
  where: AnswersWhereUniqueInput
}


/**
 * Answers deleteMany
 */
export type AnswersDeleteManyArgs = {
  where?: AnswersWhereInput
}


/**
 * Answers without action
 */
export type AnswersArgs = {
  /**
   * Select specific fields to fetch from the Answers
  **/
  select?: AnswersSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: AnswersInclude | null
}



/**
 * Model Exams
 */

export type Exams = {
  activeCode: string
  createAt: Date
  endDate: Date | null
  examDetails: string | null
  examerNick: string
  examinatedUserId: number
  examSubject: string
  genQuestionsId: number[]
  id: number
  isActive: boolean | null
  isPassed: boolean | null
  minScoreToPass: number
  score: number | null
  startDate: Date | null
}


export type AggregateExams = {
  count: number
  avg: ExamsAvgAggregateOutputType | null
  sum: ExamsSumAggregateOutputType | null
  min: ExamsMinAggregateOutputType | null
  max: ExamsMaxAggregateOutputType | null
}

export type ExamsAvgAggregateOutputType = {
  examinatedUserId: number
  genQuestionsId: number | null
  id: number
  minScoreToPass: number
  score: number | null
}

export type ExamsSumAggregateOutputType = {
  examinatedUserId: number
  genQuestionsId: number[]
  id: number
  minScoreToPass: number
  score: number | null
}

export type ExamsMinAggregateOutputType = {
  examinatedUserId: number
  genQuestionsId: number[]
  id: number
  minScoreToPass: number
  score: number | null
}

export type ExamsMaxAggregateOutputType = {
  examinatedUserId: number
  genQuestionsId: number[]
  id: number
  minScoreToPass: number
  score: number | null
}


export type ExamsAvgAggregateInputType = {
  examinatedUserId?: true
  genQuestionsId?: true
  id?: true
  minScoreToPass?: true
  score?: true
}

export type ExamsSumAggregateInputType = {
  examinatedUserId?: true
  genQuestionsId?: true
  id?: true
  minScoreToPass?: true
  score?: true
}

export type ExamsMinAggregateInputType = {
  examinatedUserId?: true
  genQuestionsId?: true
  id?: true
  minScoreToPass?: true
  score?: true
}

export type ExamsMaxAggregateInputType = {
  examinatedUserId?: true
  genQuestionsId?: true
  id?: true
  minScoreToPass?: true
  score?: true
}

export type AggregateExamsArgs = {
  where?: ExamsWhereInput
  orderBy?: Enumerable<ExamsOrderByInput> | ExamsOrderByInput
  cursor?: ExamsWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<ExamsDistinctFieldEnum>
  count?: true
  avg?: ExamsAvgAggregateInputType
  sum?: ExamsSumAggregateInputType
  min?: ExamsMinAggregateInputType
  max?: ExamsMaxAggregateInputType
}

export type GetExamsAggregateType<T extends AggregateExamsArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetExamsAggregateScalarType<T[P]>
}

export type GetExamsAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof ExamsAvgAggregateOutputType ? ExamsAvgAggregateOutputType[P] : never
}
    
    

export type ExamsSelect = {
  activeCode?: boolean
  createAt?: boolean
  endDate?: boolean
  examDetails?: boolean
  examerNick?: boolean
  examinatedUserId?: boolean
  examSubject?: boolean
  genQuestionsId?: boolean
  id?: boolean
  isActive?: boolean
  isPassed?: boolean
  minScoreToPass?: boolean
  score?: boolean
  startDate?: boolean
  User?: boolean | UserArgs
  Answers?: boolean | FindManyAnswersArgs
}

export type ExamsInclude = {
  User?: boolean | UserArgs
  Answers?: boolean | FindManyAnswersArgs
}

export type ExamsGetPayload<
  S extends boolean | null | undefined | ExamsArgs,
  U = keyof S
> = S extends true
  ? Exams
  : S extends undefined
  ? never
  : S extends ExamsArgs | FindManyExamsArgs
  ? 'include' extends U
    ? Exams  & {
      [P in TrueKeys<S['include']>]:
      P extends 'User'
      ? UserGetPayload<S['include'][P]> :
      P extends 'Answers'
      ? Array<AnswersGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Exams ? Exams[P]
: 
      P extends 'User'
      ? UserGetPayload<S['select'][P]> :
      P extends 'Answers'
      ? Array<AnswersGetPayload<S['select'][P]>> : never
    }
  : Exams
: Exams


export interface ExamsDelegate {
  /**
   * Find zero or one Exams that matches the filter.
   * @param {FindOneExamsArgs} args - Arguments to find a Exams
   * @example
   * // Get one Exams
   * const exams = await prisma.exams.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneExamsArgs>(
    args: Subset<T, FindOneExamsArgs>
  ): CheckSelect<T, Prisma__ExamsClient<Exams | null>, Prisma__ExamsClient<ExamsGetPayload<T> | null>>
  /**
   * Find the first Exams that matches the filter.
   * @param {FindFirstExamsArgs} args - Arguments to find a Exams
   * @example
   * // Get one Exams
   * const exams = await prisma.exams.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstExamsArgs>(
    args?: Subset<T, FindFirstExamsArgs>
  ): CheckSelect<T, Prisma__ExamsClient<Exams | null>, Prisma__ExamsClient<ExamsGetPayload<T> | null>>
  /**
   * Find zero or more Exams that matches the filter.
   * @param {FindManyExamsArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Exams
   * const exams = await prisma.exams.findMany()
   * 
   * // Get first 10 Exams
   * const exams = await prisma.exams.findMany({ take: 10 })
   * 
   * // Only select the `activeCode`
   * const examsWithActiveCodeOnly = await prisma.exams.findMany({ select: { activeCode: true } })
   * 
  **/
  findMany<T extends FindManyExamsArgs>(
    args?: Subset<T, FindManyExamsArgs>
  ): CheckSelect<T, Promise<Array<Exams>>, Promise<Array<ExamsGetPayload<T>>>>
  /**
   * Create a Exams.
   * @param {ExamsCreateArgs} args - Arguments to create a Exams.
   * @example
   * // Create one Exams
   * const Exams = await prisma.exams.create({
   *   data: {
   *     // ... data to create a Exams
   *   }
   * })
   * 
  **/
  create<T extends ExamsCreateArgs>(
    args: Subset<T, ExamsCreateArgs>
  ): CheckSelect<T, Prisma__ExamsClient<Exams>, Prisma__ExamsClient<ExamsGetPayload<T>>>
  /**
   * Delete a Exams.
   * @param {ExamsDeleteArgs} args - Arguments to delete one Exams.
   * @example
   * // Delete one Exams
   * const Exams = await prisma.exams.delete({
   *   where: {
   *     // ... filter to delete one Exams
   *   }
   * })
   * 
  **/
  delete<T extends ExamsDeleteArgs>(
    args: Subset<T, ExamsDeleteArgs>
  ): CheckSelect<T, Prisma__ExamsClient<Exams>, Prisma__ExamsClient<ExamsGetPayload<T>>>
  /**
   * Update one Exams.
   * @param {ExamsUpdateArgs} args - Arguments to update one Exams.
   * @example
   * // Update one Exams
   * const exams = await prisma.exams.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends ExamsUpdateArgs>(
    args: Subset<T, ExamsUpdateArgs>
  ): CheckSelect<T, Prisma__ExamsClient<Exams>, Prisma__ExamsClient<ExamsGetPayload<T>>>
  /**
   * Delete zero or more Exams.
   * @param {ExamsDeleteManyArgs} args - Arguments to filter Exams to delete.
   * @example
   * // Delete a few Exams
   * const { count } = await prisma.exams.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends ExamsDeleteManyArgs>(
    args: Subset<T, ExamsDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Exams.
   * @param {ExamsUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Exams
   * const exams = await prisma.exams.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends ExamsUpdateManyArgs>(
    args: Subset<T, ExamsUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Exams.
   * @param {ExamsUpsertArgs} args - Arguments to update or create a Exams.
   * @example
   * // Update or create a Exams
   * const exams = await prisma.exams.upsert({
   *   create: {
   *     // ... data to create a Exams
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Exams we want to update
   *   }
   * })
  **/
  upsert<T extends ExamsUpsertArgs>(
    args: Subset<T, ExamsUpsertArgs>
  ): CheckSelect<T, Prisma__ExamsClient<Exams>, Prisma__ExamsClient<ExamsGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyExamsArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateExamsArgs>(args: Subset<T, AggregateExamsArgs>): Promise<GetExamsAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Exams.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__ExamsClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  User<T extends UserArgs = {}>(args?: Subset<T, UserArgs>): CheckSelect<T, Prisma__UserClient<User | null>, Prisma__UserClient<UserGetPayload<T> | null>>;

  Answers<T extends FindManyAnswersArgs = {}>(args?: Subset<T, FindManyAnswersArgs>): CheckSelect<T, Promise<Array<Answers>>, Promise<Array<AnswersGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Exams findOne
 */
export type FindOneExamsArgs = {
  /**
   * Select specific fields to fetch from the Exams
  **/
  select?: ExamsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ExamsInclude | null
  /**
   * Filter, which Exams to fetch.
  **/
  where: ExamsWhereUniqueInput
}


/**
 * Exams findFirst
 */
export type FindFirstExamsArgs = {
  /**
   * Select specific fields to fetch from the Exams
  **/
  select?: ExamsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ExamsInclude | null
  /**
   * Filter, which Exams to fetch.
  **/
  where?: ExamsWhereInput
  orderBy?: Enumerable<ExamsOrderByInput> | ExamsOrderByInput
  cursor?: ExamsWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<ExamsDistinctFieldEnum>
}


/**
 * Exams findMany
 */
export type FindManyExamsArgs = {
  /**
   * Select specific fields to fetch from the Exams
  **/
  select?: ExamsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ExamsInclude | null
  /**
   * Filter, which Exams to fetch.
  **/
  where?: ExamsWhereInput
  /**
   * Determine the order of the Exams to fetch.
  **/
  orderBy?: Enumerable<ExamsOrderByInput> | ExamsOrderByInput
  /**
   * Sets the position for listing Exams.
  **/
  cursor?: ExamsWhereUniqueInput
  /**
   * The number of Exams to fetch. If negative number, it will take Exams before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Exams.
  **/
  skip?: number
  distinct?: Enumerable<ExamsDistinctFieldEnum>
}


/**
 * Exams create
 */
export type ExamsCreateArgs = {
  /**
   * Select specific fields to fetch from the Exams
  **/
  select?: ExamsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ExamsInclude | null
  /**
   * The data needed to create a Exams.
  **/
  data: ExamsCreateInput
}


/**
 * Exams update
 */
export type ExamsUpdateArgs = {
  /**
   * Select specific fields to fetch from the Exams
  **/
  select?: ExamsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ExamsInclude | null
  /**
   * The data needed to update a Exams.
  **/
  data: ExamsUpdateInput
  /**
   * Choose, which Exams to update.
  **/
  where: ExamsWhereUniqueInput
}


/**
 * Exams updateMany
 */
export type ExamsUpdateManyArgs = {
  data: ExamsUpdateManyMutationInput
  where?: ExamsWhereInput
}


/**
 * Exams upsert
 */
export type ExamsUpsertArgs = {
  /**
   * Select specific fields to fetch from the Exams
  **/
  select?: ExamsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ExamsInclude | null
  /**
   * The filter to search for the Exams to update in case it exists.
  **/
  where: ExamsWhereUniqueInput
  /**
   * In case the Exams found by the `where` argument doesn't exist, create a new Exams with this data.
  **/
  create: ExamsCreateInput
  /**
   * In case the Exams was found with the provided `where` argument, update it with this data.
  **/
  update: ExamsUpdateInput
}


/**
 * Exams delete
 */
export type ExamsDeleteArgs = {
  /**
   * Select specific fields to fetch from the Exams
  **/
  select?: ExamsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ExamsInclude | null
  /**
   * Filter which Exams to delete.
  **/
  where: ExamsWhereUniqueInput
}


/**
 * Exams deleteMany
 */
export type ExamsDeleteManyArgs = {
  where?: ExamsWhereInput
}


/**
 * Exams without action
 */
export type ExamsArgs = {
  /**
   * Select specific fields to fetch from the Exams
  **/
  select?: ExamsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: ExamsInclude | null
}



/**
 * Model Questions
 */

export type Questions = {
  content: string
  correctAnswer: string | null
  id: number
  image: string | null
  odpA: string | null
  odpB: string | null
  odpC: string | null
  odpD: string | null
  score: number
  type: string
}


export type AggregateQuestions = {
  count: number
  avg: QuestionsAvgAggregateOutputType | null
  sum: QuestionsSumAggregateOutputType | null
  min: QuestionsMinAggregateOutputType | null
  max: QuestionsMaxAggregateOutputType | null
}

export type QuestionsAvgAggregateOutputType = {
  id: number
  score: number
}

export type QuestionsSumAggregateOutputType = {
  id: number
  score: number
}

export type QuestionsMinAggregateOutputType = {
  id: number
  score: number
}

export type QuestionsMaxAggregateOutputType = {
  id: number
  score: number
}


export type QuestionsAvgAggregateInputType = {
  id?: true
  score?: true
}

export type QuestionsSumAggregateInputType = {
  id?: true
  score?: true
}

export type QuestionsMinAggregateInputType = {
  id?: true
  score?: true
}

export type QuestionsMaxAggregateInputType = {
  id?: true
  score?: true
}

export type AggregateQuestionsArgs = {
  where?: QuestionsWhereInput
  orderBy?: Enumerable<QuestionsOrderByInput> | QuestionsOrderByInput
  cursor?: QuestionsWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<QuestionsDistinctFieldEnum>
  count?: true
  avg?: QuestionsAvgAggregateInputType
  sum?: QuestionsSumAggregateInputType
  min?: QuestionsMinAggregateInputType
  max?: QuestionsMaxAggregateInputType
}

export type GetQuestionsAggregateType<T extends AggregateQuestionsArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetQuestionsAggregateScalarType<T[P]>
}

export type GetQuestionsAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof QuestionsAvgAggregateOutputType ? QuestionsAvgAggregateOutputType[P] : never
}
    
    

export type QuestionsSelect = {
  content?: boolean
  correctAnswer?: boolean
  id?: boolean
  image?: boolean
  odpA?: boolean
  odpB?: boolean
  odpC?: boolean
  odpD?: boolean
  score?: boolean
  type?: boolean
  Answers?: boolean | FindManyAnswersArgs
}

export type QuestionsInclude = {
  Answers?: boolean | FindManyAnswersArgs
}

export type QuestionsGetPayload<
  S extends boolean | null | undefined | QuestionsArgs,
  U = keyof S
> = S extends true
  ? Questions
  : S extends undefined
  ? never
  : S extends QuestionsArgs | FindManyQuestionsArgs
  ? 'include' extends U
    ? Questions  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Answers'
      ? Array<AnswersGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof Questions ? Questions[P]
: 
      P extends 'Answers'
      ? Array<AnswersGetPayload<S['select'][P]>> : never
    }
  : Questions
: Questions


export interface QuestionsDelegate {
  /**
   * Find zero or one Questions that matches the filter.
   * @param {FindOneQuestionsArgs} args - Arguments to find a Questions
   * @example
   * // Get one Questions
   * const questions = await prisma.questions.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneQuestionsArgs>(
    args: Subset<T, FindOneQuestionsArgs>
  ): CheckSelect<T, Prisma__QuestionsClient<Questions | null>, Prisma__QuestionsClient<QuestionsGetPayload<T> | null>>
  /**
   * Find the first Questions that matches the filter.
   * @param {FindFirstQuestionsArgs} args - Arguments to find a Questions
   * @example
   * // Get one Questions
   * const questions = await prisma.questions.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstQuestionsArgs>(
    args?: Subset<T, FindFirstQuestionsArgs>
  ): CheckSelect<T, Prisma__QuestionsClient<Questions | null>, Prisma__QuestionsClient<QuestionsGetPayload<T> | null>>
  /**
   * Find zero or more Questions that matches the filter.
   * @param {FindManyQuestionsArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Questions
   * const questions = await prisma.questions.findMany()
   * 
   * // Get first 10 Questions
   * const questions = await prisma.questions.findMany({ take: 10 })
   * 
   * // Only select the `content`
   * const questionsWithContentOnly = await prisma.questions.findMany({ select: { content: true } })
   * 
  **/
  findMany<T extends FindManyQuestionsArgs>(
    args?: Subset<T, FindManyQuestionsArgs>
  ): CheckSelect<T, Promise<Array<Questions>>, Promise<Array<QuestionsGetPayload<T>>>>
  /**
   * Create a Questions.
   * @param {QuestionsCreateArgs} args - Arguments to create a Questions.
   * @example
   * // Create one Questions
   * const Questions = await prisma.questions.create({
   *   data: {
   *     // ... data to create a Questions
   *   }
   * })
   * 
  **/
  create<T extends QuestionsCreateArgs>(
    args: Subset<T, QuestionsCreateArgs>
  ): CheckSelect<T, Prisma__QuestionsClient<Questions>, Prisma__QuestionsClient<QuestionsGetPayload<T>>>
  /**
   * Delete a Questions.
   * @param {QuestionsDeleteArgs} args - Arguments to delete one Questions.
   * @example
   * // Delete one Questions
   * const Questions = await prisma.questions.delete({
   *   where: {
   *     // ... filter to delete one Questions
   *   }
   * })
   * 
  **/
  delete<T extends QuestionsDeleteArgs>(
    args: Subset<T, QuestionsDeleteArgs>
  ): CheckSelect<T, Prisma__QuestionsClient<Questions>, Prisma__QuestionsClient<QuestionsGetPayload<T>>>
  /**
   * Update one Questions.
   * @param {QuestionsUpdateArgs} args - Arguments to update one Questions.
   * @example
   * // Update one Questions
   * const questions = await prisma.questions.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends QuestionsUpdateArgs>(
    args: Subset<T, QuestionsUpdateArgs>
  ): CheckSelect<T, Prisma__QuestionsClient<Questions>, Prisma__QuestionsClient<QuestionsGetPayload<T>>>
  /**
   * Delete zero or more Questions.
   * @param {QuestionsDeleteManyArgs} args - Arguments to filter Questions to delete.
   * @example
   * // Delete a few Questions
   * const { count } = await prisma.questions.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends QuestionsDeleteManyArgs>(
    args: Subset<T, QuestionsDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Questions.
   * @param {QuestionsUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Questions
   * const questions = await prisma.questions.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends QuestionsUpdateManyArgs>(
    args: Subset<T, QuestionsUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one Questions.
   * @param {QuestionsUpsertArgs} args - Arguments to update or create a Questions.
   * @example
   * // Update or create a Questions
   * const questions = await prisma.questions.upsert({
   *   create: {
   *     // ... data to create a Questions
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the Questions we want to update
   *   }
   * })
  **/
  upsert<T extends QuestionsUpsertArgs>(
    args: Subset<T, QuestionsUpsertArgs>
  ): CheckSelect<T, Prisma__QuestionsClient<Questions>, Prisma__QuestionsClient<QuestionsGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyQuestionsArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateQuestionsArgs>(args: Subset<T, AggregateQuestionsArgs>): Promise<GetQuestionsAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for Questions.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__QuestionsClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Answers<T extends FindManyAnswersArgs = {}>(args?: Subset<T, FindManyAnswersArgs>): CheckSelect<T, Promise<Array<Answers>>, Promise<Array<AnswersGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * Questions findOne
 */
export type FindOneQuestionsArgs = {
  /**
   * Select specific fields to fetch from the Questions
  **/
  select?: QuestionsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: QuestionsInclude | null
  /**
   * Filter, which Questions to fetch.
  **/
  where: QuestionsWhereUniqueInput
}


/**
 * Questions findFirst
 */
export type FindFirstQuestionsArgs = {
  /**
   * Select specific fields to fetch from the Questions
  **/
  select?: QuestionsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: QuestionsInclude | null
  /**
   * Filter, which Questions to fetch.
  **/
  where?: QuestionsWhereInput
  orderBy?: Enumerable<QuestionsOrderByInput> | QuestionsOrderByInput
  cursor?: QuestionsWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<QuestionsDistinctFieldEnum>
}


/**
 * Questions findMany
 */
export type FindManyQuestionsArgs = {
  /**
   * Select specific fields to fetch from the Questions
  **/
  select?: QuestionsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: QuestionsInclude | null
  /**
   * Filter, which Questions to fetch.
  **/
  where?: QuestionsWhereInput
  /**
   * Determine the order of the Questions to fetch.
  **/
  orderBy?: Enumerable<QuestionsOrderByInput> | QuestionsOrderByInput
  /**
   * Sets the position for listing Questions.
  **/
  cursor?: QuestionsWhereUniqueInput
  /**
   * The number of Questions to fetch. If negative number, it will take Questions before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Questions.
  **/
  skip?: number
  distinct?: Enumerable<QuestionsDistinctFieldEnum>
}


/**
 * Questions create
 */
export type QuestionsCreateArgs = {
  /**
   * Select specific fields to fetch from the Questions
  **/
  select?: QuestionsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: QuestionsInclude | null
  /**
   * The data needed to create a Questions.
  **/
  data: QuestionsCreateInput
}


/**
 * Questions update
 */
export type QuestionsUpdateArgs = {
  /**
   * Select specific fields to fetch from the Questions
  **/
  select?: QuestionsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: QuestionsInclude | null
  /**
   * The data needed to update a Questions.
  **/
  data: QuestionsUpdateInput
  /**
   * Choose, which Questions to update.
  **/
  where: QuestionsWhereUniqueInput
}


/**
 * Questions updateMany
 */
export type QuestionsUpdateManyArgs = {
  data: QuestionsUpdateManyMutationInput
  where?: QuestionsWhereInput
}


/**
 * Questions upsert
 */
export type QuestionsUpsertArgs = {
  /**
   * Select specific fields to fetch from the Questions
  **/
  select?: QuestionsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: QuestionsInclude | null
  /**
   * The filter to search for the Questions to update in case it exists.
  **/
  where: QuestionsWhereUniqueInput
  /**
   * In case the Questions found by the `where` argument doesn't exist, create a new Questions with this data.
  **/
  create: QuestionsCreateInput
  /**
   * In case the Questions was found with the provided `where` argument, update it with this data.
  **/
  update: QuestionsUpdateInput
}


/**
 * Questions delete
 */
export type QuestionsDeleteArgs = {
  /**
   * Select specific fields to fetch from the Questions
  **/
  select?: QuestionsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: QuestionsInclude | null
  /**
   * Filter which Questions to delete.
  **/
  where: QuestionsWhereUniqueInput
}


/**
 * Questions deleteMany
 */
export type QuestionsDeleteManyArgs = {
  where?: QuestionsWhereInput
}


/**
 * Questions without action
 */
export type QuestionsArgs = {
  /**
   * Select specific fields to fetch from the Questions
  **/
  select?: QuestionsSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: QuestionsInclude | null
}



/**
 * Model User
 */

export type User = {
  createAt: Date
  email: string
  id: number
  isActive: boolean
  isOnExam: boolean
  nick: string
  password: string
  role: number
}


export type AggregateUser = {
  count: number
  avg: UserAvgAggregateOutputType | null
  sum: UserSumAggregateOutputType | null
  min: UserMinAggregateOutputType | null
  max: UserMaxAggregateOutputType | null
}

export type UserAvgAggregateOutputType = {
  id: number
  role: number
}

export type UserSumAggregateOutputType = {
  id: number
  role: number
}

export type UserMinAggregateOutputType = {
  id: number
  role: number
}

export type UserMaxAggregateOutputType = {
  id: number
  role: number
}


export type UserAvgAggregateInputType = {
  id?: true
  role?: true
}

export type UserSumAggregateInputType = {
  id?: true
  role?: true
}

export type UserMinAggregateInputType = {
  id?: true
  role?: true
}

export type UserMaxAggregateInputType = {
  id?: true
  role?: true
}

export type AggregateUserArgs = {
  where?: UserWhereInput
  orderBy?: Enumerable<UserOrderByInput> | UserOrderByInput
  cursor?: UserWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<UserDistinctFieldEnum>
  count?: true
  avg?: UserAvgAggregateInputType
  sum?: UserSumAggregateInputType
  min?: UserMinAggregateInputType
  max?: UserMaxAggregateInputType
}

export type GetUserAggregateType<T extends AggregateUserArgs> = {
  [P in keyof T]: P extends 'count' ? number : GetUserAggregateScalarType<T[P]>
}

export type GetUserAggregateScalarType<T extends any> = {
  [P in keyof T]: P extends keyof UserAvgAggregateOutputType ? UserAvgAggregateOutputType[P] : never
}
    
    

export type UserSelect = {
  createAt?: boolean
  email?: boolean
  id?: boolean
  isActive?: boolean
  isOnExam?: boolean
  nick?: boolean
  password?: boolean
  role?: boolean
  Answers?: boolean | FindManyAnswersArgs
  Exams?: boolean | FindManyExamsArgs
}

export type UserInclude = {
  Answers?: boolean | FindManyAnswersArgs
  Exams?: boolean | FindManyExamsArgs
}

export type UserGetPayload<
  S extends boolean | null | undefined | UserArgs,
  U = keyof S
> = S extends true
  ? User
  : S extends undefined
  ? never
  : S extends UserArgs | FindManyUserArgs
  ? 'include' extends U
    ? User  & {
      [P in TrueKeys<S['include']>]:
      P extends 'Answers'
      ? Array<AnswersGetPayload<S['include'][P]>> :
      P extends 'Exams'
      ? Array<ExamsGetPayload<S['include'][P]>> : never
    }
  : 'select' extends U
    ? {
      [P in TrueKeys<S['select']>]:P extends keyof User ? User[P]
: 
      P extends 'Answers'
      ? Array<AnswersGetPayload<S['select'][P]>> :
      P extends 'Exams'
      ? Array<ExamsGetPayload<S['select'][P]>> : never
    }
  : User
: User


export interface UserDelegate {
  /**
   * Find zero or one User that matches the filter.
   * @param {FindOneUserArgs} args - Arguments to find a User
   * @example
   * // Get one User
   * const user = await prisma.user.findOne({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findOne<T extends FindOneUserArgs>(
    args: Subset<T, FindOneUserArgs>
  ): CheckSelect<T, Prisma__UserClient<User | null>, Prisma__UserClient<UserGetPayload<T> | null>>
  /**
   * Find the first User that matches the filter.
   * @param {FindFirstUserArgs} args - Arguments to find a User
   * @example
   * // Get one User
   * const user = await prisma.user.findFirst({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
  **/
  findFirst<T extends FindFirstUserArgs>(
    args?: Subset<T, FindFirstUserArgs>
  ): CheckSelect<T, Prisma__UserClient<User | null>, Prisma__UserClient<UserGetPayload<T> | null>>
  /**
   * Find zero or more Users that matches the filter.
   * @param {FindManyUserArgs=} args - Arguments to filter and select certain fields only.
   * @example
   * // Get all Users
   * const users = await prisma.user.findMany()
   * 
   * // Get first 10 Users
   * const users = await prisma.user.findMany({ take: 10 })
   * 
   * // Only select the `createAt`
   * const userWithCreateAtOnly = await prisma.user.findMany({ select: { createAt: true } })
   * 
  **/
  findMany<T extends FindManyUserArgs>(
    args?: Subset<T, FindManyUserArgs>
  ): CheckSelect<T, Promise<Array<User>>, Promise<Array<UserGetPayload<T>>>>
  /**
   * Create a User.
   * @param {UserCreateArgs} args - Arguments to create a User.
   * @example
   * // Create one User
   * const User = await prisma.user.create({
   *   data: {
   *     // ... data to create a User
   *   }
   * })
   * 
  **/
  create<T extends UserCreateArgs>(
    args: Subset<T, UserCreateArgs>
  ): CheckSelect<T, Prisma__UserClient<User>, Prisma__UserClient<UserGetPayload<T>>>
  /**
   * Delete a User.
   * @param {UserDeleteArgs} args - Arguments to delete one User.
   * @example
   * // Delete one User
   * const User = await prisma.user.delete({
   *   where: {
   *     // ... filter to delete one User
   *   }
   * })
   * 
  **/
  delete<T extends UserDeleteArgs>(
    args: Subset<T, UserDeleteArgs>
  ): CheckSelect<T, Prisma__UserClient<User>, Prisma__UserClient<UserGetPayload<T>>>
  /**
   * Update one User.
   * @param {UserUpdateArgs} args - Arguments to update one User.
   * @example
   * // Update one User
   * const user = await prisma.user.update({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  update<T extends UserUpdateArgs>(
    args: Subset<T, UserUpdateArgs>
  ): CheckSelect<T, Prisma__UserClient<User>, Prisma__UserClient<UserGetPayload<T>>>
  /**
   * Delete zero or more Users.
   * @param {UserDeleteManyArgs} args - Arguments to filter Users to delete.
   * @example
   * // Delete a few Users
   * const { count } = await prisma.user.deleteMany({
   *   where: {
   *     // ... provide filter here
   *   }
   * })
   * 
  **/
  deleteMany<T extends UserDeleteManyArgs>(
    args: Subset<T, UserDeleteManyArgs>
  ): Promise<BatchPayload>
  /**
   * Update zero or more Users.
   * @param {UserUpdateManyArgs} args - Arguments to update one or more rows.
   * @example
   * // Update many Users
   * const user = await prisma.user.updateMany({
   *   where: {
   *     // ... provide filter here
   *   },
   *   data: {
   *     // ... provide data here
   *   }
   * })
   * 
  **/
  updateMany<T extends UserUpdateManyArgs>(
    args: Subset<T, UserUpdateManyArgs>
  ): Promise<BatchPayload>
  /**
   * Create or update one User.
   * @param {UserUpsertArgs} args - Arguments to update or create a User.
   * @example
   * // Update or create a User
   * const user = await prisma.user.upsert({
   *   create: {
   *     // ... data to create a User
   *   },
   *   update: {
   *     // ... in case it already exists, update
   *   },
   *   where: {
   *     // ... the filter for the User we want to update
   *   }
   * })
  **/
  upsert<T extends UserUpsertArgs>(
    args: Subset<T, UserUpsertArgs>
  ): CheckSelect<T, Prisma__UserClient<User>, Prisma__UserClient<UserGetPayload<T>>>
  /**
   * Count
   */
  count(args?: Omit<FindManyUserArgs, 'select' | 'include'>): Promise<number>

  /**
   * Aggregate
   */
  aggregate<T extends AggregateUserArgs>(args: Subset<T, AggregateUserArgs>): Promise<GetUserAggregateType<T>>
}

/**
 * The delegate class that acts as a "Promise-like" for User.
 * Why is this prefixed with `Prisma__`?
 * Because we want to prevent naming conflicts as mentioned in 
 * https://github.com/prisma/prisma-client-js/issues/707
 */
export declare class Prisma__UserClient<T> implements Promise<T> {
  private readonly _dmmf;
  private readonly _fetcher;
  private readonly _queryType;
  private readonly _rootField;
  private readonly _clientMethod;
  private readonly _args;
  private readonly _dataPath;
  private readonly _errorFormat;
  private readonly _measurePerformance?;
  private _isList;
  private _callsite;
  private _requestPromise?;
  constructor(_dmmf: DMMFClass, _fetcher: PrismaClientFetcher, _queryType: 'query' | 'mutation', _rootField: string, _clientMethod: string, _args: any, _dataPath: string[], _errorFormat: ErrorFormat, _measurePerformance?: boolean | undefined, _isList?: boolean);
  readonly [Symbol.toStringTag]: 'PrismaClientPromise';

  Answers<T extends FindManyAnswersArgs = {}>(args?: Subset<T, FindManyAnswersArgs>): CheckSelect<T, Promise<Array<Answers>>, Promise<Array<AnswersGetPayload<T>>>>;

  Exams<T extends FindManyExamsArgs = {}>(args?: Subset<T, FindManyExamsArgs>): CheckSelect<T, Promise<Array<Exams>>, Promise<Array<ExamsGetPayload<T>>>>;

  private get _document();
  /**
   * Attaches callbacks for the resolution and/or rejection of the Promise.
   * @param onfulfilled The callback to execute when the Promise is resolved.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of which ever callback is executed.
   */
  then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | Promise<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | Promise<TResult2>) | undefined | null): Promise<TResult1 | TResult2>;
  /**
   * Attaches a callback for only the rejection of the Promise.
   * @param onrejected The callback to execute when the Promise is rejected.
   * @returns A Promise for the completion of the callback.
   */
  catch<TResult = never>(onrejected?: ((reason: any) => TResult | Promise<TResult>) | undefined | null): Promise<T | TResult>;
  /**
   * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
   * resolved value cannot be modified from the callback.
   * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
   * @returns A Promise for the completion of the callback.
   */
  finally(onfinally?: (() => void) | undefined | null): Promise<T>;
}

// Custom InputTypes

/**
 * User findOne
 */
export type FindOneUserArgs = {
  /**
   * Select specific fields to fetch from the User
  **/
  select?: UserSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: UserInclude | null
  /**
   * Filter, which User to fetch.
  **/
  where: UserWhereUniqueInput
}


/**
 * User findFirst
 */
export type FindFirstUserArgs = {
  /**
   * Select specific fields to fetch from the User
  **/
  select?: UserSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: UserInclude | null
  /**
   * Filter, which User to fetch.
  **/
  where?: UserWhereInput
  orderBy?: Enumerable<UserOrderByInput> | UserOrderByInput
  cursor?: UserWhereUniqueInput
  take?: number
  skip?: number
  distinct?: Enumerable<UserDistinctFieldEnum>
}


/**
 * User findMany
 */
export type FindManyUserArgs = {
  /**
   * Select specific fields to fetch from the User
  **/
  select?: UserSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: UserInclude | null
  /**
   * Filter, which Users to fetch.
  **/
  where?: UserWhereInput
  /**
   * Determine the order of the Users to fetch.
  **/
  orderBy?: Enumerable<UserOrderByInput> | UserOrderByInput
  /**
   * Sets the position for listing Users.
  **/
  cursor?: UserWhereUniqueInput
  /**
   * The number of Users to fetch. If negative number, it will take Users before the `cursor`.
  **/
  take?: number
  /**
   * Skip the first `n` Users.
  **/
  skip?: number
  distinct?: Enumerable<UserDistinctFieldEnum>
}


/**
 * User create
 */
export type UserCreateArgs = {
  /**
   * Select specific fields to fetch from the User
  **/
  select?: UserSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: UserInclude | null
  /**
   * The data needed to create a User.
  **/
  data: UserCreateInput
}


/**
 * User update
 */
export type UserUpdateArgs = {
  /**
   * Select specific fields to fetch from the User
  **/
  select?: UserSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: UserInclude | null
  /**
   * The data needed to update a User.
  **/
  data: UserUpdateInput
  /**
   * Choose, which User to update.
  **/
  where: UserWhereUniqueInput
}


/**
 * User updateMany
 */
export type UserUpdateManyArgs = {
  data: UserUpdateManyMutationInput
  where?: UserWhereInput
}


/**
 * User upsert
 */
export type UserUpsertArgs = {
  /**
   * Select specific fields to fetch from the User
  **/
  select?: UserSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: UserInclude | null
  /**
   * The filter to search for the User to update in case it exists.
  **/
  where: UserWhereUniqueInput
  /**
   * In case the User found by the `where` argument doesn't exist, create a new User with this data.
  **/
  create: UserCreateInput
  /**
   * In case the User was found with the provided `where` argument, update it with this data.
  **/
  update: UserUpdateInput
}


/**
 * User delete
 */
export type UserDeleteArgs = {
  /**
   * Select specific fields to fetch from the User
  **/
  select?: UserSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: UserInclude | null
  /**
   * Filter which User to delete.
  **/
  where: UserWhereUniqueInput
}


/**
 * User deleteMany
 */
export type UserDeleteManyArgs = {
  where?: UserWhereInput
}


/**
 * User without action
 */
export type UserArgs = {
  /**
   * Select specific fields to fetch from the User
  **/
  select?: UserSelect | null
  /**
   * Choose, which related nodes to fetch as well.
  **/
  include?: UserInclude | null
}



/**
 * Deep Input Types
 */


export type AnswersWhereInput = {
  AND?: AnswersWhereInput | Enumerable<AnswersWhereInput>
  OR?: AnswersWhereInput | Enumerable<AnswersWhereInput>
  NOT?: AnswersWhereInput | Enumerable<AnswersWhereInput>
  answer?: StringFilter | string
  answeredAt?: DateTimeFilter | Date | string
  examerNote?: StringNullableFilter | string | null
  examId?: IntFilter | number
  examinatedUserId?: IntNullableFilter | number | null
  getScored?: IntNullableFilter | number | null
  id?: IntFilter | number
  isCorrectAnswer?: BoolNullableFilter | boolean | null
  isRated?: BoolNullableFilter | boolean | null
  question?: StringFilter | string
  questionId?: IntFilter | number
  Exams?: ExamsRelationFilter | ExamsWhereInput
  User?: UserRelationFilter | UserWhereInput | null
  Questions?: QuestionsRelationFilter | QuestionsWhereInput
}

export type AnswersOrderByInput = {
  answer?: SortOrder
  answeredAt?: SortOrder
  examerNote?: SortOrder
  examId?: SortOrder
  examinatedUserId?: SortOrder
  getScored?: SortOrder
  id?: SortOrder
  isCorrectAnswer?: SortOrder
  isRated?: SortOrder
  question?: SortOrder
  questionId?: SortOrder
}

export type AnswersWhereUniqueInput = {
  examId?: number
  examinatedUserId?: number
  id?: number
  questionId?: number
}

export type ExamsWhereInput = {
  AND?: ExamsWhereInput | Enumerable<ExamsWhereInput>
  OR?: ExamsWhereInput | Enumerable<ExamsWhereInput>
  NOT?: ExamsWhereInput | Enumerable<ExamsWhereInput>
  activeCode?: StringFilter | string
  createAt?: DateTimeFilter | Date | string
  endDate?: DateTimeNullableFilter | Date | string | null
  examDetails?: StringNullableFilter | string | null
  examerNick?: StringFilter | string
  examinatedUserId?: IntFilter | number
  examSubject?: StringFilter | string
  genQuestionsId?: IntNullableListFilter
  id?: IntFilter | number
  isActive?: BoolNullableFilter | boolean | null
  isPassed?: BoolNullableFilter | boolean | null
  minScoreToPass?: IntFilter | number
  score?: IntNullableFilter | number | null
  startDate?: DateTimeNullableFilter | Date | string | null
  User?: UserRelationFilter | UserWhereInput
  Answers?: AnswersListRelationFilter
}

export type ExamsOrderByInput = {
  activeCode?: SortOrder
  createAt?: SortOrder
  endDate?: SortOrder
  examDetails?: SortOrder
  examerNick?: SortOrder
  examinatedUserId?: SortOrder
  examSubject?: SortOrder
  genQuestionsId?: SortOrder
  id?: SortOrder
  isActive?: SortOrder
  isPassed?: SortOrder
  minScoreToPass?: SortOrder
  score?: SortOrder
  startDate?: SortOrder
}

export type ExamsWhereUniqueInput = {
  examinatedUserId?: number
  id?: number
}

export type QuestionsWhereInput = {
  AND?: QuestionsWhereInput | Enumerable<QuestionsWhereInput>
  OR?: QuestionsWhereInput | Enumerable<QuestionsWhereInput>
  NOT?: QuestionsWhereInput | Enumerable<QuestionsWhereInput>
  content?: StringFilter | string
  correctAnswer?: StringNullableFilter | string | null
  id?: IntFilter | number
  image?: StringNullableFilter | string | null
  odpA?: StringNullableFilter | string | null
  odpB?: StringNullableFilter | string | null
  odpC?: StringNullableFilter | string | null
  odpD?: StringNullableFilter | string | null
  score?: IntFilter | number
  type?: StringFilter | string
  Answers?: AnswersListRelationFilter
}

export type QuestionsOrderByInput = {
  content?: SortOrder
  correctAnswer?: SortOrder
  id?: SortOrder
  image?: SortOrder
  odpA?: SortOrder
  odpB?: SortOrder
  odpC?: SortOrder
  odpD?: SortOrder
  score?: SortOrder
  type?: SortOrder
}

export type QuestionsWhereUniqueInput = {
  id?: number
}

export type UserWhereInput = {
  AND?: UserWhereInput | Enumerable<UserWhereInput>
  OR?: UserWhereInput | Enumerable<UserWhereInput>
  NOT?: UserWhereInput | Enumerable<UserWhereInput>
  createAt?: DateTimeFilter | Date | string
  email?: StringFilter | string
  id?: IntFilter | number
  isActive?: BoolFilter | boolean
  isOnExam?: BoolFilter | boolean
  nick?: StringFilter | string
  password?: StringFilter | string
  role?: IntFilter | number
  Answers?: AnswersListRelationFilter
  Exams?: ExamsListRelationFilter
}

export type UserOrderByInput = {
  createAt?: SortOrder
  email?: SortOrder
  id?: SortOrder
  isActive?: SortOrder
  isOnExam?: SortOrder
  nick?: SortOrder
  password?: SortOrder
  role?: SortOrder
}

export type UserWhereUniqueInput = {
  id?: number
}

export type AnswersCreateInput = {
  answer: string
  answeredAt?: Date | string
  examerNote?: string | null
  getScored?: number | null
  isCorrectAnswer?: boolean | null
  isRated?: boolean | null
  question: string
  Exams: ExamsCreateOneWithoutAnswersInput
  User?: UserCreateOneWithoutAnswersInput
  Questions: QuestionsCreateOneWithoutAnswersInput
}

export type AnswersUpdateInput = {
  answer?: string | StringFieldUpdateOperationsInput
  answeredAt?: Date | string | DateTimeFieldUpdateOperationsInput
  examerNote?: string | NullableStringFieldUpdateOperationsInput | null
  getScored?: number | NullableIntFieldUpdateOperationsInput | null
  isCorrectAnswer?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isRated?: boolean | NullableBoolFieldUpdateOperationsInput | null
  question?: string | StringFieldUpdateOperationsInput
  Exams?: ExamsUpdateOneRequiredWithoutAnswersInput
  User?: UserUpdateOneWithoutAnswersInput
  Questions?: QuestionsUpdateOneRequiredWithoutAnswersInput
}

export type AnswersUpdateManyMutationInput = {
  answer?: string | StringFieldUpdateOperationsInput
  answeredAt?: Date | string | DateTimeFieldUpdateOperationsInput
  examerNote?: string | NullableStringFieldUpdateOperationsInput | null
  getScored?: number | NullableIntFieldUpdateOperationsInput | null
  isCorrectAnswer?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isRated?: boolean | NullableBoolFieldUpdateOperationsInput | null
  question?: string | StringFieldUpdateOperationsInput
}

export type ExamsCreateInput = {
  activeCode: string
  createAt?: Date | string
  endDate?: Date | string | null
  examDetails?: string | null
  examerNick: string
  examSubject: string
  isActive?: boolean | null
  isPassed?: boolean | null
  minScoreToPass: number
  score?: number | null
  startDate?: Date | string | null
  genQuestionsId?: ExamsCreategenQuestionsIdInput | Enumerable<number>
  User: UserCreateOneWithoutExamsInput
  Answers?: AnswersCreateManyWithoutExamsInput
}

export type ExamsUpdateInput = {
  activeCode?: string | StringFieldUpdateOperationsInput
  createAt?: Date | string | DateTimeFieldUpdateOperationsInput
  endDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  examDetails?: string | NullableStringFieldUpdateOperationsInput | null
  examerNick?: string | StringFieldUpdateOperationsInput
  examSubject?: string | StringFieldUpdateOperationsInput
  isActive?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isPassed?: boolean | NullableBoolFieldUpdateOperationsInput | null
  minScoreToPass?: number | IntFieldUpdateOperationsInput
  score?: number | NullableIntFieldUpdateOperationsInput | null
  startDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  genQuestionsId?: ExamsUpdategenQuestionsIdInput | Enumerable<number>
  User?: UserUpdateOneRequiredWithoutExamsInput
  Answers?: AnswersUpdateManyWithoutExamsInput
}

export type ExamsUpdateManyMutationInput = {
  activeCode?: string | StringFieldUpdateOperationsInput
  createAt?: Date | string | DateTimeFieldUpdateOperationsInput
  endDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  examDetails?: string | NullableStringFieldUpdateOperationsInput | null
  examerNick?: string | StringFieldUpdateOperationsInput
  examSubject?: string | StringFieldUpdateOperationsInput
  isActive?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isPassed?: boolean | NullableBoolFieldUpdateOperationsInput | null
  minScoreToPass?: number | IntFieldUpdateOperationsInput
  score?: number | NullableIntFieldUpdateOperationsInput | null
  startDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  genQuestionsId?: ExamsUpdategenQuestionsIdInput | Enumerable<number>
}

export type QuestionsCreateInput = {
  content: string
  correctAnswer?: string | null
  image?: string | null
  odpA?: string | null
  odpB?: string | null
  odpC?: string | null
  odpD?: string | null
  score: number
  type: string
  Answers?: AnswersCreateManyWithoutQuestionsInput
}

export type QuestionsUpdateInput = {
  content?: string | StringFieldUpdateOperationsInput
  correctAnswer?: string | NullableStringFieldUpdateOperationsInput | null
  image?: string | NullableStringFieldUpdateOperationsInput | null
  odpA?: string | NullableStringFieldUpdateOperationsInput | null
  odpB?: string | NullableStringFieldUpdateOperationsInput | null
  odpC?: string | NullableStringFieldUpdateOperationsInput | null
  odpD?: string | NullableStringFieldUpdateOperationsInput | null
  score?: number | IntFieldUpdateOperationsInput
  type?: string | StringFieldUpdateOperationsInput
  Answers?: AnswersUpdateManyWithoutQuestionsInput
}

export type QuestionsUpdateManyMutationInput = {
  content?: string | StringFieldUpdateOperationsInput
  correctAnswer?: string | NullableStringFieldUpdateOperationsInput | null
  image?: string | NullableStringFieldUpdateOperationsInput | null
  odpA?: string | NullableStringFieldUpdateOperationsInput | null
  odpB?: string | NullableStringFieldUpdateOperationsInput | null
  odpC?: string | NullableStringFieldUpdateOperationsInput | null
  odpD?: string | NullableStringFieldUpdateOperationsInput | null
  score?: number | IntFieldUpdateOperationsInput
  type?: string | StringFieldUpdateOperationsInput
}

export type UserCreateInput = {
  createAt?: Date | string
  email: string
  isActive?: boolean
  isOnExam?: boolean
  nick: string
  password: string
  role?: number
  Answers?: AnswersCreateManyWithoutUserInput
  Exams?: ExamsCreateManyWithoutUserInput
}

export type UserUpdateInput = {
  createAt?: Date | string | DateTimeFieldUpdateOperationsInput
  email?: string | StringFieldUpdateOperationsInput
  isActive?: boolean | BoolFieldUpdateOperationsInput
  isOnExam?: boolean | BoolFieldUpdateOperationsInput
  nick?: string | StringFieldUpdateOperationsInput
  password?: string | StringFieldUpdateOperationsInput
  role?: number | IntFieldUpdateOperationsInput
  Answers?: AnswersUpdateManyWithoutUserInput
  Exams?: ExamsUpdateManyWithoutUserInput
}

export type UserUpdateManyMutationInput = {
  createAt?: Date | string | DateTimeFieldUpdateOperationsInput
  email?: string | StringFieldUpdateOperationsInput
  isActive?: boolean | BoolFieldUpdateOperationsInput
  isOnExam?: boolean | BoolFieldUpdateOperationsInput
  nick?: string | StringFieldUpdateOperationsInput
  password?: string | StringFieldUpdateOperationsInput
  role?: number | IntFieldUpdateOperationsInput
}

export type StringFilter = {
  equals?: string
  in?: Enumerable<string>
  notIn?: Enumerable<string>
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  mode?: QueryMode
  not?: string | NestedStringFilter
}

export type DateTimeFilter = {
  equals?: Date | string
  in?: Enumerable<Date> | Enumerable<string>
  notIn?: Enumerable<Date> | Enumerable<string>
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: Date | string | NestedDateTimeFilter
}

export type StringNullableFilter = {
  equals?: string | null
  in?: Enumerable<string> | null
  notIn?: Enumerable<string> | null
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  mode?: QueryMode
  not?: string | NestedStringNullableFilter | null
}

export type IntFilter = {
  equals?: number
  in?: Enumerable<number>
  notIn?: Enumerable<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedIntFilter
}

export type IntNullableFilter = {
  equals?: number | null
  in?: Enumerable<number> | null
  notIn?: Enumerable<number> | null
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedIntNullableFilter | null
}

export type BoolNullableFilter = {
  equals?: boolean | null
  not?: boolean | NestedBoolNullableFilter | null
}

export type ExamsRelationFilter = {
  is?: ExamsWhereInput
  isNot?: ExamsWhereInput
}

export type UserRelationFilter = {
  is?: UserWhereInput
  isNot?: UserWhereInput
}

export type QuestionsRelationFilter = {
  is?: QuestionsWhereInput
  isNot?: QuestionsWhereInput
}

export type DateTimeNullableFilter = {
  equals?: Date | string | null
  in?: Enumerable<Date> | Enumerable<string> | null
  notIn?: Enumerable<Date> | Enumerable<string> | null
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: Date | string | NestedDateTimeNullableFilter | null
}

export type IntNullableListFilter = {
  equals?: Enumerable<number> | null
}

export type AnswersListRelationFilter = {
  every?: AnswersWhereInput
  some?: AnswersWhereInput
  none?: AnswersWhereInput
}

export type BoolFilter = {
  equals?: boolean
  not?: boolean | NestedBoolFilter
}

export type ExamsListRelationFilter = {
  every?: ExamsWhereInput
  some?: ExamsWhereInput
  none?: ExamsWhereInput
}

export type ExamsCreateOneWithoutAnswersInput = {
  create?: ExamsCreateWithoutAnswersInput
  connect?: ExamsWhereUniqueInput
}

export type UserCreateOneWithoutAnswersInput = {
  create?: UserCreateWithoutAnswersInput
  connect?: UserWhereUniqueInput
}

export type QuestionsCreateOneWithoutAnswersInput = {
  create?: QuestionsCreateWithoutAnswersInput
  connect?: QuestionsWhereUniqueInput
}

export type StringFieldUpdateOperationsInput = {
  set?: string
}

export type DateTimeFieldUpdateOperationsInput = {
  set?: Date | string
}

export type NullableStringFieldUpdateOperationsInput = {
  set?: string | null
}

export type NullableIntFieldUpdateOperationsInput = {
  set?: number | null
}

export type NullableBoolFieldUpdateOperationsInput = {
  set?: boolean | null
}

export type ExamsUpdateOneRequiredWithoutAnswersInput = {
  create?: ExamsCreateWithoutAnswersInput
  connect?: ExamsWhereUniqueInput
  update?: ExamsUpdateWithoutAnswersDataInput
  upsert?: ExamsUpsertWithoutAnswersInput
}

export type UserUpdateOneWithoutAnswersInput = {
  create?: UserCreateWithoutAnswersInput
  connect?: UserWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: UserUpdateWithoutAnswersDataInput
  upsert?: UserUpsertWithoutAnswersInput
}

export type QuestionsUpdateOneRequiredWithoutAnswersInput = {
  create?: QuestionsCreateWithoutAnswersInput
  connect?: QuestionsWhereUniqueInput
  update?: QuestionsUpdateWithoutAnswersDataInput
  upsert?: QuestionsUpsertWithoutAnswersInput
}

export type ExamsCreategenQuestionsIdInput = {
  set: Enumerable<number>
}

export type UserCreateOneWithoutExamsInput = {
  create?: UserCreateWithoutExamsInput
  connect?: UserWhereUniqueInput
}

export type AnswersCreateManyWithoutExamsInput = {
  create?: AnswersCreateWithoutExamsInput | Enumerable<AnswersCreateWithoutExamsInput>
  connect?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
}

export type NullableDateTimeFieldUpdateOperationsInput = {
  set?: Date | string | null
}

export type IntFieldUpdateOperationsInput = {
  set?: number
}

export type ExamsUpdategenQuestionsIdInput = {
  set: Enumerable<number>
}

export type UserUpdateOneRequiredWithoutExamsInput = {
  create?: UserCreateWithoutExamsInput
  connect?: UserWhereUniqueInput
  update?: UserUpdateWithoutExamsDataInput
  upsert?: UserUpsertWithoutExamsInput
}

export type AnswersUpdateManyWithoutExamsInput = {
  create?: AnswersCreateWithoutExamsInput | Enumerable<AnswersCreateWithoutExamsInput>
  connect?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  set?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  disconnect?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  delete?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  update?: AnswersUpdateWithWhereUniqueWithoutExamsInput | Enumerable<AnswersUpdateWithWhereUniqueWithoutExamsInput>
  updateMany?: AnswersUpdateManyWithWhereNestedInput | Enumerable<AnswersUpdateManyWithWhereNestedInput>
  deleteMany?: AnswersScalarWhereInput | Enumerable<AnswersScalarWhereInput>
  upsert?: AnswersUpsertWithWhereUniqueWithoutExamsInput | Enumerable<AnswersUpsertWithWhereUniqueWithoutExamsInput>
}

export type AnswersCreateManyWithoutQuestionsInput = {
  create?: AnswersCreateWithoutQuestionsInput | Enumerable<AnswersCreateWithoutQuestionsInput>
  connect?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
}

export type AnswersUpdateManyWithoutQuestionsInput = {
  create?: AnswersCreateWithoutQuestionsInput | Enumerable<AnswersCreateWithoutQuestionsInput>
  connect?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  set?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  disconnect?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  delete?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  update?: AnswersUpdateWithWhereUniqueWithoutQuestionsInput | Enumerable<AnswersUpdateWithWhereUniqueWithoutQuestionsInput>
  updateMany?: AnswersUpdateManyWithWhereNestedInput | Enumerable<AnswersUpdateManyWithWhereNestedInput>
  deleteMany?: AnswersScalarWhereInput | Enumerable<AnswersScalarWhereInput>
  upsert?: AnswersUpsertWithWhereUniqueWithoutQuestionsInput | Enumerable<AnswersUpsertWithWhereUniqueWithoutQuestionsInput>
}

export type AnswersCreateManyWithoutUserInput = {
  create?: AnswersCreateWithoutUserInput | Enumerable<AnswersCreateWithoutUserInput>
  connect?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
}

export type ExamsCreateManyWithoutUserInput = {
  create?: ExamsCreateWithoutUserInput | Enumerable<ExamsCreateWithoutUserInput>
  connect?: ExamsWhereUniqueInput | Enumerable<ExamsWhereUniqueInput>
}

export type BoolFieldUpdateOperationsInput = {
  set?: boolean
}

export type AnswersUpdateManyWithoutUserInput = {
  create?: AnswersCreateWithoutUserInput | Enumerable<AnswersCreateWithoutUserInput>
  connect?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  set?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  disconnect?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  delete?: AnswersWhereUniqueInput | Enumerable<AnswersWhereUniqueInput>
  update?: AnswersUpdateWithWhereUniqueWithoutUserInput | Enumerable<AnswersUpdateWithWhereUniqueWithoutUserInput>
  updateMany?: AnswersUpdateManyWithWhereNestedInput | Enumerable<AnswersUpdateManyWithWhereNestedInput>
  deleteMany?: AnswersScalarWhereInput | Enumerable<AnswersScalarWhereInput>
  upsert?: AnswersUpsertWithWhereUniqueWithoutUserInput | Enumerable<AnswersUpsertWithWhereUniqueWithoutUserInput>
}

export type ExamsUpdateManyWithoutUserInput = {
  create?: ExamsCreateWithoutUserInput | Enumerable<ExamsCreateWithoutUserInput>
  connect?: ExamsWhereUniqueInput | Enumerable<ExamsWhereUniqueInput>
  set?: ExamsWhereUniqueInput | Enumerable<ExamsWhereUniqueInput>
  disconnect?: ExamsWhereUniqueInput | Enumerable<ExamsWhereUniqueInput>
  delete?: ExamsWhereUniqueInput | Enumerable<ExamsWhereUniqueInput>
  update?: ExamsUpdateWithWhereUniqueWithoutUserInput | Enumerable<ExamsUpdateWithWhereUniqueWithoutUserInput>
  updateMany?: ExamsUpdateManyWithWhereNestedInput | Enumerable<ExamsUpdateManyWithWhereNestedInput>
  deleteMany?: ExamsScalarWhereInput | Enumerable<ExamsScalarWhereInput>
  upsert?: ExamsUpsertWithWhereUniqueWithoutUserInput | Enumerable<ExamsUpsertWithWhereUniqueWithoutUserInput>
}

export type NestedStringFilter = {
  equals?: string
  in?: Enumerable<string>
  notIn?: Enumerable<string>
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  not?: string | NestedStringFilter
}

export type NestedDateTimeFilter = {
  equals?: Date | string
  in?: Enumerable<Date> | Enumerable<string>
  notIn?: Enumerable<Date> | Enumerable<string>
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: Date | string | NestedDateTimeFilter
}

export type NestedStringNullableFilter = {
  equals?: string | null
  in?: Enumerable<string> | null
  notIn?: Enumerable<string> | null
  lt?: string
  lte?: string
  gt?: string
  gte?: string
  contains?: string
  startsWith?: string
  endsWith?: string
  not?: string | NestedStringNullableFilter | null
}

export type NestedIntFilter = {
  equals?: number
  in?: Enumerable<number>
  notIn?: Enumerable<number>
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedIntFilter
}

export type NestedIntNullableFilter = {
  equals?: number | null
  in?: Enumerable<number> | null
  notIn?: Enumerable<number> | null
  lt?: number
  lte?: number
  gt?: number
  gte?: number
  not?: number | NestedIntNullableFilter | null
}

export type NestedBoolNullableFilter = {
  equals?: boolean | null
  not?: boolean | NestedBoolNullableFilter | null
}

export type NestedDateTimeNullableFilter = {
  equals?: Date | string | null
  in?: Enumerable<Date> | Enumerable<string> | null
  notIn?: Enumerable<Date> | Enumerable<string> | null
  lt?: Date | string
  lte?: Date | string
  gt?: Date | string
  gte?: Date | string
  not?: Date | string | NestedDateTimeNullableFilter | null
}

export type NestedBoolFilter = {
  equals?: boolean
  not?: boolean | NestedBoolFilter
}

export type ExamsCreateWithoutAnswersInput = {
  activeCode: string
  createAt?: Date | string
  endDate?: Date | string | null
  examDetails?: string | null
  examerNick: string
  examSubject: string
  isActive?: boolean | null
  isPassed?: boolean | null
  minScoreToPass: number
  score?: number | null
  startDate?: Date | string | null
  genQuestionsId?: ExamsCreategenQuestionsIdInput | Enumerable<number>
  User: UserCreateOneWithoutExamsInput
}

export type UserCreateWithoutAnswersInput = {
  createAt?: Date | string
  email: string
  isActive?: boolean
  isOnExam?: boolean
  nick: string
  password: string
  role?: number
  Exams?: ExamsCreateManyWithoutUserInput
}

export type QuestionsCreateWithoutAnswersInput = {
  content: string
  correctAnswer?: string | null
  image?: string | null
  odpA?: string | null
  odpB?: string | null
  odpC?: string | null
  odpD?: string | null
  score: number
  type: string
}

export type ExamsUpdateWithoutAnswersDataInput = {
  activeCode?: string | StringFieldUpdateOperationsInput
  createAt?: Date | string | DateTimeFieldUpdateOperationsInput
  endDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  examDetails?: string | NullableStringFieldUpdateOperationsInput | null
  examerNick?: string | StringFieldUpdateOperationsInput
  examSubject?: string | StringFieldUpdateOperationsInput
  isActive?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isPassed?: boolean | NullableBoolFieldUpdateOperationsInput | null
  minScoreToPass?: number | IntFieldUpdateOperationsInput
  score?: number | NullableIntFieldUpdateOperationsInput | null
  startDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  genQuestionsId?: ExamsUpdategenQuestionsIdInput | Enumerable<number>
  User?: UserUpdateOneRequiredWithoutExamsInput
}

export type ExamsUpsertWithoutAnswersInput = {
  update: ExamsUpdateWithoutAnswersDataInput
  create: ExamsCreateWithoutAnswersInput
}

export type UserUpdateWithoutAnswersDataInput = {
  createAt?: Date | string | DateTimeFieldUpdateOperationsInput
  email?: string | StringFieldUpdateOperationsInput
  isActive?: boolean | BoolFieldUpdateOperationsInput
  isOnExam?: boolean | BoolFieldUpdateOperationsInput
  nick?: string | StringFieldUpdateOperationsInput
  password?: string | StringFieldUpdateOperationsInput
  role?: number | IntFieldUpdateOperationsInput
  Exams?: ExamsUpdateManyWithoutUserInput
}

export type UserUpsertWithoutAnswersInput = {
  update: UserUpdateWithoutAnswersDataInput
  create: UserCreateWithoutAnswersInput
}

export type QuestionsUpdateWithoutAnswersDataInput = {
  content?: string | StringFieldUpdateOperationsInput
  correctAnswer?: string | NullableStringFieldUpdateOperationsInput | null
  image?: string | NullableStringFieldUpdateOperationsInput | null
  odpA?: string | NullableStringFieldUpdateOperationsInput | null
  odpB?: string | NullableStringFieldUpdateOperationsInput | null
  odpC?: string | NullableStringFieldUpdateOperationsInput | null
  odpD?: string | NullableStringFieldUpdateOperationsInput | null
  score?: number | IntFieldUpdateOperationsInput
  type?: string | StringFieldUpdateOperationsInput
}

export type QuestionsUpsertWithoutAnswersInput = {
  update: QuestionsUpdateWithoutAnswersDataInput
  create: QuestionsCreateWithoutAnswersInput
}

export type UserCreateWithoutExamsInput = {
  createAt?: Date | string
  email: string
  isActive?: boolean
  isOnExam?: boolean
  nick: string
  password: string
  role?: number
  Answers?: AnswersCreateManyWithoutUserInput
}

export type AnswersCreateWithoutExamsInput = {
  answer: string
  answeredAt?: Date | string
  examerNote?: string | null
  getScored?: number | null
  isCorrectAnswer?: boolean | null
  isRated?: boolean | null
  question: string
  User?: UserCreateOneWithoutAnswersInput
  Questions: QuestionsCreateOneWithoutAnswersInput
}

export type UserUpdateWithoutExamsDataInput = {
  createAt?: Date | string | DateTimeFieldUpdateOperationsInput
  email?: string | StringFieldUpdateOperationsInput
  isActive?: boolean | BoolFieldUpdateOperationsInput
  isOnExam?: boolean | BoolFieldUpdateOperationsInput
  nick?: string | StringFieldUpdateOperationsInput
  password?: string | StringFieldUpdateOperationsInput
  role?: number | IntFieldUpdateOperationsInput
  Answers?: AnswersUpdateManyWithoutUserInput
}

export type UserUpsertWithoutExamsInput = {
  update: UserUpdateWithoutExamsDataInput
  create: UserCreateWithoutExamsInput
}

export type AnswersUpdateWithWhereUniqueWithoutExamsInput = {
  where: AnswersWhereUniqueInput
  data: AnswersUpdateWithoutExamsDataInput
}

export type AnswersUpdateManyWithWhereNestedInput = {
  where: AnswersScalarWhereInput
  data: AnswersUpdateManyDataInput
}

export type AnswersScalarWhereInput = {
  AND?: AnswersScalarWhereInput | Enumerable<AnswersScalarWhereInput>
  OR?: AnswersScalarWhereInput | Enumerable<AnswersScalarWhereInput>
  NOT?: AnswersScalarWhereInput | Enumerable<AnswersScalarWhereInput>
  answer?: StringFilter | string
  answeredAt?: DateTimeFilter | Date | string
  examerNote?: StringNullableFilter | string | null
  examId?: IntFilter | number
  examinatedUserId?: IntNullableFilter | number | null
  getScored?: IntNullableFilter | number | null
  id?: IntFilter | number
  isCorrectAnswer?: BoolNullableFilter | boolean | null
  isRated?: BoolNullableFilter | boolean | null
  question?: StringFilter | string
  questionId?: IntFilter | number
}

export type AnswersUpsertWithWhereUniqueWithoutExamsInput = {
  where: AnswersWhereUniqueInput
  update: AnswersUpdateWithoutExamsDataInput
  create: AnswersCreateWithoutExamsInput
}

export type AnswersCreateWithoutQuestionsInput = {
  answer: string
  answeredAt?: Date | string
  examerNote?: string | null
  getScored?: number | null
  isCorrectAnswer?: boolean | null
  isRated?: boolean | null
  question: string
  Exams: ExamsCreateOneWithoutAnswersInput
  User?: UserCreateOneWithoutAnswersInput
}

export type AnswersUpdateWithWhereUniqueWithoutQuestionsInput = {
  where: AnswersWhereUniqueInput
  data: AnswersUpdateWithoutQuestionsDataInput
}

export type AnswersUpsertWithWhereUniqueWithoutQuestionsInput = {
  where: AnswersWhereUniqueInput
  update: AnswersUpdateWithoutQuestionsDataInput
  create: AnswersCreateWithoutQuestionsInput
}

export type AnswersCreateWithoutUserInput = {
  answer: string
  answeredAt?: Date | string
  examerNote?: string | null
  getScored?: number | null
  isCorrectAnswer?: boolean | null
  isRated?: boolean | null
  question: string
  Exams: ExamsCreateOneWithoutAnswersInput
  Questions: QuestionsCreateOneWithoutAnswersInput
}

export type ExamsCreateWithoutUserInput = {
  activeCode: string
  createAt?: Date | string
  endDate?: Date | string | null
  examDetails?: string | null
  examerNick: string
  examSubject: string
  isActive?: boolean | null
  isPassed?: boolean | null
  minScoreToPass: number
  score?: number | null
  startDate?: Date | string | null
  genQuestionsId?: ExamsCreategenQuestionsIdInput | Enumerable<number>
  Answers?: AnswersCreateManyWithoutExamsInput
}

export type AnswersUpdateWithWhereUniqueWithoutUserInput = {
  where: AnswersWhereUniqueInput
  data: AnswersUpdateWithoutUserDataInput
}

export type AnswersUpsertWithWhereUniqueWithoutUserInput = {
  where: AnswersWhereUniqueInput
  update: AnswersUpdateWithoutUserDataInput
  create: AnswersCreateWithoutUserInput
}

export type ExamsUpdateWithWhereUniqueWithoutUserInput = {
  where: ExamsWhereUniqueInput
  data: ExamsUpdateWithoutUserDataInput
}

export type ExamsUpdateManyWithWhereNestedInput = {
  where: ExamsScalarWhereInput
  data: ExamsUpdateManyDataInput
}

export type ExamsScalarWhereInput = {
  AND?: ExamsScalarWhereInput | Enumerable<ExamsScalarWhereInput>
  OR?: ExamsScalarWhereInput | Enumerable<ExamsScalarWhereInput>
  NOT?: ExamsScalarWhereInput | Enumerable<ExamsScalarWhereInput>
  activeCode?: StringFilter | string
  createAt?: DateTimeFilter | Date | string
  endDate?: DateTimeNullableFilter | Date | string | null
  examDetails?: StringNullableFilter | string | null
  examerNick?: StringFilter | string
  examinatedUserId?: IntFilter | number
  examSubject?: StringFilter | string
  genQuestionsId?: IntNullableListFilter
  id?: IntFilter | number
  isActive?: BoolNullableFilter | boolean | null
  isPassed?: BoolNullableFilter | boolean | null
  minScoreToPass?: IntFilter | number
  score?: IntNullableFilter | number | null
  startDate?: DateTimeNullableFilter | Date | string | null
}

export type ExamsUpsertWithWhereUniqueWithoutUserInput = {
  where: ExamsWhereUniqueInput
  update: ExamsUpdateWithoutUserDataInput
  create: ExamsCreateWithoutUserInput
}

export type AnswersUpdateWithoutExamsDataInput = {
  answer?: string | StringFieldUpdateOperationsInput
  answeredAt?: Date | string | DateTimeFieldUpdateOperationsInput
  examerNote?: string | NullableStringFieldUpdateOperationsInput | null
  getScored?: number | NullableIntFieldUpdateOperationsInput | null
  isCorrectAnswer?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isRated?: boolean | NullableBoolFieldUpdateOperationsInput | null
  question?: string | StringFieldUpdateOperationsInput
  User?: UserUpdateOneWithoutAnswersInput
  Questions?: QuestionsUpdateOneRequiredWithoutAnswersInput
}

export type AnswersUpdateManyDataInput = {
  answer?: string | StringFieldUpdateOperationsInput
  answeredAt?: Date | string | DateTimeFieldUpdateOperationsInput
  examerNote?: string | NullableStringFieldUpdateOperationsInput | null
  getScored?: number | NullableIntFieldUpdateOperationsInput | null
  isCorrectAnswer?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isRated?: boolean | NullableBoolFieldUpdateOperationsInput | null
  question?: string | StringFieldUpdateOperationsInput
}

export type AnswersUpdateWithoutQuestionsDataInput = {
  answer?: string | StringFieldUpdateOperationsInput
  answeredAt?: Date | string | DateTimeFieldUpdateOperationsInput
  examerNote?: string | NullableStringFieldUpdateOperationsInput | null
  getScored?: number | NullableIntFieldUpdateOperationsInput | null
  isCorrectAnswer?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isRated?: boolean | NullableBoolFieldUpdateOperationsInput | null
  question?: string | StringFieldUpdateOperationsInput
  Exams?: ExamsUpdateOneRequiredWithoutAnswersInput
  User?: UserUpdateOneWithoutAnswersInput
}

export type AnswersUpdateWithoutUserDataInput = {
  answer?: string | StringFieldUpdateOperationsInput
  answeredAt?: Date | string | DateTimeFieldUpdateOperationsInput
  examerNote?: string | NullableStringFieldUpdateOperationsInput | null
  getScored?: number | NullableIntFieldUpdateOperationsInput | null
  isCorrectAnswer?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isRated?: boolean | NullableBoolFieldUpdateOperationsInput | null
  question?: string | StringFieldUpdateOperationsInput
  Exams?: ExamsUpdateOneRequiredWithoutAnswersInput
  Questions?: QuestionsUpdateOneRequiredWithoutAnswersInput
}

export type ExamsUpdateWithoutUserDataInput = {
  activeCode?: string | StringFieldUpdateOperationsInput
  createAt?: Date | string | DateTimeFieldUpdateOperationsInput
  endDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  examDetails?: string | NullableStringFieldUpdateOperationsInput | null
  examerNick?: string | StringFieldUpdateOperationsInput
  examSubject?: string | StringFieldUpdateOperationsInput
  isActive?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isPassed?: boolean | NullableBoolFieldUpdateOperationsInput | null
  minScoreToPass?: number | IntFieldUpdateOperationsInput
  score?: number | NullableIntFieldUpdateOperationsInput | null
  startDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  genQuestionsId?: ExamsUpdategenQuestionsIdInput | Enumerable<number>
  Answers?: AnswersUpdateManyWithoutExamsInput
}

export type ExamsUpdateManyDataInput = {
  activeCode?: string | StringFieldUpdateOperationsInput
  createAt?: Date | string | DateTimeFieldUpdateOperationsInput
  endDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  examDetails?: string | NullableStringFieldUpdateOperationsInput | null
  examerNick?: string | StringFieldUpdateOperationsInput
  examSubject?: string | StringFieldUpdateOperationsInput
  isActive?: boolean | NullableBoolFieldUpdateOperationsInput | null
  isPassed?: boolean | NullableBoolFieldUpdateOperationsInput | null
  minScoreToPass?: number | IntFieldUpdateOperationsInput
  score?: number | NullableIntFieldUpdateOperationsInput | null
  startDate?: Date | string | NullableDateTimeFieldUpdateOperationsInput | null
  genQuestionsId?: ExamsUpdategenQuestionsIdInput | Enumerable<number>
}

/**
 * Batch Payload for updateMany & deleteMany
 */

export type BatchPayload = {
  count: number
}

/**
 * DMMF
 */
export declare const dmmf: DMMF.Document;
export {};
