interface IFilterObject<T> {
    where?: T,
    fields: string[],
    filterValue: string
}
export const createFilterObjectFromFields = <T>({ where, fields, filterValue }: IFilterObject<T>) => {
    const namesOfFields = [];
    if (fields) {
        for (const val of fields) {
            namesOfFields.push({
                [val]: filterValue
            });
        }
    }
    return namesOfFields.length > 0 ? { ...where, OR: [...namesOfFields] } : where;
}