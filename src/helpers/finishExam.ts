import { AuthenticationError } from "apollo-server-express";
import { IContext } from "../types/context";
import { PrismaClient, PrismaClientOptions } from "@prisma/client";

export const finishExam = async (req: Express.Request, db: PrismaClient<PrismaClientOptions, never>) => {
    if (!req.session!.isOnExam) {
        throw new AuthenticationError('Nie jesteś na egzaminie')
    }
    req.session!.isOnExam = null;
    req.session!.examId = null;
    req.session!.genQuestionsId = null;
    req.session!.examDetails = null;
    await db.user.update({
        data: {
            isOnExam: false,
        },
        where: {
            id: +(req.session!.userId)
        }
    })
    return "Egzamin zakończony";

}