import { ValidationError } from "apollo-server-express";

export const genIDs = (IDs: number[], IDsToGen: number) => {
    const temp: number[] = []
    if (IDs.length % IDsToGen !== 0) throw new ValidationError('Liczba pytań niepozwala na równy podział');
    while (temp.length < IDsToGen) {
        const randIdxIDs = Math.floor(Math.random() * IDs.length);
        if (!temp.includes(IDs[randIdxIDs])) {
            temp.push(IDs[randIdxIDs])
        }
    }
    return temp;
}