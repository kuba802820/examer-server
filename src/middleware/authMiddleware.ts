import { IContext } from "../types/context";
import { AuthenticationError } from "apollo-server-express";

export enum Roles {
    USER = 0,
    EXAMER = 1,
    ADMIN = 2
}

export const isAuthenticated = (next: any, minRequiredRole: Roles = Roles.USER) => (_: any, args: any, ctx: IContext) => {
    if (!ctx.req.session!.isLogin || ctx.req.session!.role < minRequiredRole) {
        throw new AuthenticationError('Upewnij się że jesteś zalogowany, lub posiadasz uprawnienia')
    }
    return next(_, args, ctx);
}
