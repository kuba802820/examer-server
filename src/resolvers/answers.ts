import { isAuthenticated, Roles } from "../middleware/authMiddleware";
import { IContext } from "../types/context";
import { ValidationError, ApolloError } from "apollo-server-express";
import { AnswerQuestion, RateTheAnswer, AnswersByExamId, AnswerById } from "../types/answers";
import { finishExam } from "../helpers/finishExam";
import { PaginationAndFiltering } from "../types/utils";
export default {
    Query: {
        getAnswersByExamId: isAuthenticated(async (_: any, args: PaginationAndFiltering & AnswersByExamId, { db }: IContext) => {
            const Answers = await db.answers.findMany({
                include: {
                    Questions: {
                        include: {
                            Answers: true,
                        }
                    }
                },
                where: {
                    examId: args.examId,
                    isRated: args.isRated || false,
                },
                take: args.take,
                skip: args.skip
            });
            return Answers;
        }, Roles.EXAMER),
    },
    Mutation: {
        answerQuestion: isAuthenticated(async (_: any, args: AnswerQuestion, { db, req }: IContext) => {
            if (!req.session!.isOnExam) throw new ValidationError("Nie jesteś na egzaminie");
            if (!req.session!.questionId) throw new ValidationError("Nie masz aktywnego żadnego pytania");
            const UserAnswers = await db.answers.findMany({
                where: {
                    examId: +(req.session!.examId),
                    answer: args.answer,
                    examinatedUserId: +(req.session!.userId),
                    question: req.session!.questionContent,
                }
            });
            if (UserAnswers.length > 0) throw new ValidationError("Już odpowiedziałeś na to pytanie")
            const Exam = await db.exams.findOne({
                where: {
                    id: +(req.session!.examId),
                }
            })
            if (Exam?.endDate) {
                if (Date.now() > new Date(Exam?.endDate as Date).getTime()) {
                    await finishExam(req, db);
                    throw new ApolloError("Czas egzaminu dobiegł końca! - Dodawanie odpowiedzi jest zablokowane", "EXAM_EXPIRED");
                }
            }
            await db.answers.create({
                data: {
                    answer: args.answer,
                    question: req.session!.questionContent,
                    User: {
                        connect: {
                            id: +(req.session!.userId)
                        }
                    },
                    Exams: {
                        connect: {
                            id: +(req.session!.examId)
                        }
                    },
                    Questions: {
                        connect: {
                            id: +(req.session!.questionId)
                        }
                    }
                }
            });

            return true;
        }),
        rateTheAnswer: isAuthenticated(async (_: any, args: RateTheAnswer, { db }: IContext) => {
            const RatedAnswer = await db.answers.update({
                data: {
                    getScored: args.getScored,
                    isCorrectAnswer: args.isCorrectAnswer,
                    examerNote: args.examerNote,
                    isRated: true,
                },
                where: {
                    id: args.answerId,
                }
            })
            const Exam = await db.exams.findOne({
                where: {
                    id: RatedAnswer.examId
                }
            })
            const [{ sum }] = await db.queryRaw(`SELECT SUM("getScored") FROM public."Answers" WHERE "examId" = ${RatedAnswer.examId};`)
            const isExamPass = sum > Exam!.minScoreToPass ? true : false;
            await db.exams.update({
                data: {
                    isPassed: isExamPass,
                    score: sum,
                },
                where: {
                    id: RatedAnswer.examId
                }
            })
            return RatedAnswer;
        }, Roles.EXAMER)
    },
}