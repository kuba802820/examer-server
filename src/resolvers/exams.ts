import { ValidationError } from "apollo-server-express";
import { IContext } from "../types/context";
import randomstring from 'randomstring';
import { AddExam, AssignToExam, UserExamId } from "../types/exams";
import { Exams } from "@prisma/client";
import { Roles, isAuthenticated } from "../middleware/authMiddleware";
import { finishExam } from "../helpers/finishExam";
import { PaginationAndFiltering } from "../types/utils";

const checkExamStatus = (Exam: Exams[]) => {
    const EXAM_IS_UP = Date.now() > new Date(Exam[0]?.endDate as Date).getTime();
    const EXAM_IS_NOT_STARTED = Date.now() < new Date(Exam[0]?.startDate as Date).getTime();
    return EXAM_IS_UP ? 'Termin egzaminu już minął' : EXAM_IS_NOT_STARTED ? 'Egzamin jeszcze nie rozpoczęty' : true;
}

export default {
    Query: {
        getExamById: isAuthenticated(async (_: any, args: UserExamId, { db }: IContext) => {
            const Exam = await db.exams.findOne({
                where: {
                    id: args.userId,
                }
            })
            return Exam;
        }, Roles.EXAMER),
        getExamsByUserId: isAuthenticated(async (_: any, args: UserExamId, { db, req }: IContext) => {
            const Exams = await db.exams.findMany({
                where: {
                    examinatedUserId: args.userId || +(req.session!.userId),
                },
                orderBy: {
                    createAt: 'desc'
                },
            })
            return Exams;
        }, Roles.EXAMER),
        getExamsByLoginUser: isAuthenticated(async (_: any, args: PaginationAndFiltering, { db, req }: IContext) => {
            const Exams = await db.exams.findMany({
                skip: args.skip,
                take: args.take,
                include: {
                    Answers: {
                        include: {
                            Questions: true,
                        }
                    }
                },
                where: {
                    examinatedUserId: +(req.session!.userId)
                },
                orderBy: {
                    createAt: 'desc'
                },
            })

            return Exams;
        }),
        getAllExams: isAuthenticated(async (_: any, args: PaginationAndFiltering, { db }: IContext) => {
            const Exams = await db.exams.findMany({
                skip: args.skip,
                take: args.take,
                include: {
                    User: true,
                    Answers: true
                },
                orderBy: {
                    createAt: 'desc'
                },
            });
            return Exams;
        }, Roles.EXAMER)

    },
    Mutation: {
        assignToExam: isAuthenticated(async (_: any, args: AssignToExam, { db, req }: IContext) => {
            const Exam = await db.exams.findMany({
                where: {
                    activeCode: args.activeCode,
                    id: args.examId,
                    isActive: false
                },
                take: 1,
            });
            const User = await db.user.findOne({
                where: {
                    id: +(req.session!.userId)
                }
            })
            if ((Exam[0]?.startDate) !== null || Exam[0]?.endDate !== null) {
                const EXAM_STATUS = checkExamStatus(Exam);
                if (EXAM_STATUS !== true) throw new ValidationError(EXAM_STATUS as string)
            };
            if (User?.isOnExam) throw new ValidationError("Już masz aktywny egzamin!");
            if (Exam.length <= 0) throw new ValidationError("Upewnij się że kod jest poprawny, lub czy nie został już użyty");
            await db.exams.update({
                data: {
                    isActive: true,
                    User: {
                        update: {
                            isOnExam: true
                        },
                        connect: {
                            id: +(req.session!.userId)
                        }
                    }
                },
                where: {
                    id: args.examId,
                },
            });
            req.session!.isOnExam = true;
            req.session!.examId = Exam[0].id;
            req.session!.genQuestionsId = JSON.stringify(Exam[0].genQuestionsId);
            req.session!.examDetails = Exam[0].examDetails;
            req.session!.startDate = Exam[0].startDate || "null";
            req.session!.endDate = Exam[0].endDate || "null";
            return Exam;
        }),

        addExam: isAuthenticated(async (_: any, args: AddExam, { db, req }: IContext) => {
            const User = await db.user.findOne({
                where: {
                    id: args.examinatedUserId
                }
            })
            if (!User) return new ValidationError('Nie znaleziono użytkownika o podanym id')
            console.log(args.endDate);
            const Exam = await db.exams.create({
                data: {
                    examerNick: req.session!.nick,
                    examSubject: args.examSubject,
                    activeCode: randomstring.generate({ length: 5, charset: 'alphanumeric', }),
                    startDate: !isNaN(args.startDate) ? new Date(args.startDate * 1000) : null,
                    endDate: !isNaN(args.endDate) ? new Date(args.endDate * 1000) : null,
                    examDetails: args.examDetails,
                    minScoreToPass: args.minScoreToPass,
                    genQuestionsId: {
                        set: args.questionIds
                    },
                    User: {
                        connect: {
                            id: args.examinatedUserId
                        }
                    }
                }
            })
            return Exam;
        }, Roles.EXAMER),
        deleteExam: isAuthenticated(async (_: any, args: { id: number }, { db }: IContext) => {
            await db.answers.deleteMany({
                where: {
                    examId: args.id
                }
            })
            await db.exams.delete({
                where: {
                    id: args.id
                }
            })
            return true;
        }, Roles.ADMIN),
        finishExam: isAuthenticated(
            async (_: any, { db, req }: IContext) => finishExam(req, db)
        ),
        finishExamByUserId: isAuthenticated(async (_: any, args: { id: number }, { db }: IContext) => {
            await db.user.update({
                data: {
                    isOnExam: false
                },
                where: {
                    id: args.id
                }
            });
            return true;
        }, Roles.EXAMER)
    }
}