import userResolver from './user'
import examResolver from './exams'
import questionResolver from './questions'
import answersResolver from './answers'


export default [userResolver, examResolver, questionResolver, answersResolver];