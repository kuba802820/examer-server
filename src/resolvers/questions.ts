import { IContext } from "../types/context";
import { ValidationError } from "apollo-server-express";
import { GenRandomQuestion, QuestionId, Question } from "../types/questions";
import { genIDs } from "../helpers/genUniqueQuestionId";
import { Roles, isAuthenticated } from "../middleware/authMiddleware";
import { PaginationAndFiltering } from "../types/utils";

const getAndUpdateIdQuestion = (questionsForExam: number[], req: Express.Request) => {
    const firstIdQuestion = questionsForExam[0];
    questionsForExam.shift();
    req.session!.genQuestionsId = JSON.stringify(questionsForExam)
    return firstIdQuestion;
}
export default {
    Query: {
        getQuestionById: isAuthenticated(async (_: any, args: QuestionId, { validationErrors, db, req }: IContext) => {
            const Question = await db.questions.findOne({
                where: {
                    id: args.id
                }
            })
            if (!Question) throw new ValidationError("Nie znaleziono pytania")
            return Question;
        }),
        getAllQuestions: isAuthenticated(async (_: any, args: PaginationAndFiltering, { db }: IContext) => {
            if (args.skip < 0) throw new ValidationError("Nie możesz cofnąć strony");
            const Questions = await db.questions.findMany({
                skip: args.skip,
                take: args.take
            })

            return Questions;
        }, Roles.EXAMER)
    },
    Mutation: {
        addQuestion: isAuthenticated(async (_: any, args: Question, { db }: IContext) => {
            const Question = await db.questions.create({
                data: {
                    content: args.content,
                    score: args.score,
                    type: args.type,
                    correctAnswer: args.correctAnswer,
                    odpA: args.odpA,
                    odpB: args.odpB,
                    odpC: args.odpC,
                    odpD: args.odpD,
                    image: args.image
                }
            })
            return Question;
        }, Roles.ADMIN),
        getRandomQuestion: isAuthenticated(async (_: any, args: GenRandomQuestion, { db, req }: IContext) => {
            const IdQuestions = await db.questions.findMany({
                select: {
                    id: true,
                }
            });
            if (IdQuestions.length <= 0) throw new ValidationError('Brak pytań w bazie!')
            const randomquestionIDs = genIDs(IdQuestions.map((v: { id: number; }) => v.id), args.questionsToGen as number);
            req.session!.questionsId = randomquestionIDs;
            return randomquestionIDs;
        }, Roles.EXAMER),
        deleteQuestion: isAuthenticated(async (_: any, args: QuestionId, { db }: IContext) => {
            try {
                const AnswersByQuestion = await db.answers.count({
                    where: {
                        questionId: args.id
                    }
                })
                if (AnswersByQuestion > 0) await db.answers.delete({
                    where: {
                        questionId: args.id
                    }
                })
                await db.questions.delete({
                    where: {
                        id: args.id
                    }
                });
                return true;
            } catch (error) {
                throw new Error(error)
            }
        }, Roles.ADMIN),
        updateQuestion: isAuthenticated(async (_: any, args: Question & QuestionId, { db }: IContext) => {
            try {
                const UpdatedQuestion = await db.questions.update({
                    data: args,
                    where: {
                        id: args.id
                    }
                })
                return UpdatedQuestion;
            } catch (error) {
                throw new Error(error)
            }
        }, Roles.ADMIN),
        getNextQuestion: isAuthenticated(async (_: any, args: any, { db, req }: IContext) => {
            if (!req.session!.isOnExam) throw new ValidationError("Nie jesteś na egzaminie");
            const questionsForExam: number[] = JSON.parse(req.session!.genQuestionsId);
            const actualQuestionId = getAndUpdateIdQuestion(questionsForExam, req);
            if (actualQuestionId) {
                const Question = await db.questions.findOne({
                    where: {
                        id: actualQuestionId
                    }
                });
                if (!Question) throw new ValidationError("Nie znaleziono pytania")
                req.session!.questionContent = Question?.content;
                req.session!.questionId = Question?.id;
                return Question;
            }
        }),
    }
}