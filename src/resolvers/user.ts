// import { AuthenticationError, ForbiddenError, ValidationError } from "apollo-server-express";
// import { IContext } from "../types/context";
// import { User } from "../types/user";
// import bcrypt from 'bcrypt';
// import { isAuthenticated, Roles } from "../middleware/authMiddleware";
// import { PaginationAndFiltering } from "../types/utils";
// import { validate, validator } from "graphql-validation";

// export default {
//     Query: {
//         me: isAuthenticated(async (_: any, args: any, { req, db }: IContext) => {
//             return {
//                 email: req.session!.email,
//                 nick: req.session!.nick,
//                 role: req.session!.role,
//             }
//         }),
//         getUserById: isAuthenticated(async (_: any, args: Pick<User, 'id'>, { req, db }: IContext) => {
//             const User = await db.user.findOne({
//                 where: {
//                     id: args.id
//                 }
//             })
//             return User;
//         }, Roles.EXAMER),
//         getAllUsers: isAuthenticated(async (_: any, args: PaginationAndFiltering, { req, db }: IContext) => {
//             const Users = await db.user.findMany({
//                 take: args.take,
//                 skip: args.skip,
//             })
//             return Users;
//         }, Roles.EXAMER),
//     },
//     Mutation: {
//         loginUser: async (_: any, args: Pick<User, 'email' | 'password'>, { db, req }: IContext) => {
//             const User = await db.user.findMany({
//                 where: {
//                     email: args.email
//                 },
//                 take: 1
//             });
//             if (User.length > 0) {
//                 const correctPassword = await bcrypt.compare(args.password, User[0].password);
//                 if (!correctPassword || User.length === 0) throw new AuthenticationError("Niepoprawny email lub hasło")
//                 const { email, nick, role, id } = User[0];
//                 req.session!.isLogin = true;
//                 req.session!.email = email;
//                 req.session!.nick = nick;
//                 req.session!.role = role;
//                 req.session!.userId = id.toString();
//                 return { email, nick, role, id };
//             } else {
//                 throw new AuthenticationError("Niepoprawny email lub hasło")
//             }

//         },
//         logoutUser: async (_: any, args: any, { req }: IContext) => {
//             if (req.session!.isLogin) {
//                 req.session!.destroy((err) => err);
//                 return true;
//             }
//             return false;
//         },
//         createUser: validator([
//             validate('password').isLength({ msg: "Hasło wymagane", options: { min: 1 } }),
//             validate('email')
//                 .isEmail({ msg: "Niepoprawny email" }),
//             validate('nick').isLength({ msg: "Nick musi mieć od 1 do 64 znaków", options: { min: 1, max: 64 } })
//         ], async (_: any, args: Pick<User, 'email' | 'password' | 'nick'>, { validationErrors, db }: IContext) => {
//             if (validationErrors) throw new ValidationError(JSON.stringify(validationErrors));
//             const numbersOfRegisteredEmail = await db.user.count({ where: { email: args.email } })
//             if (numbersOfRegisteredEmail >= 1) throw new ForbiddenError('Podany email już istnieje w bazie danych')
//             const User = await db.user.create({
//                 data: {
//                     email: args.email,
//                     nick: args.nick,
//                     password: await bcrypt.hash(args.password, 10),
//                     role: 1
//                 }
//             });
//             return User;
//         }),
//         deleteUser: isAuthenticated(async (_: any, args: Pick<User, 'id'>, { db, req }: IContext) => {
//             try {
//                 await db.user.delete({
//                     where: {
//                         id: args.id
//                     }
//                 })
//                 return true;
//             } catch (error) {
//                 throw new Error(error)
//             }
//         }, Roles.ADMIN),
//         updateUser: isAuthenticated(async (_: any, args: User, { db, req }: IContext) => {
//             try {
//                 const UpdatedUser = await db.user.update({
//                     data: args as Omit<User, 'id'>,
//                     where: {
//                         id: args.id
//                     }
//                 })
//                 return UpdatedUser;
//             } catch (error) {
//                 throw new Error(error)
//             }
//         }, Roles.ADMIN)
//     }
// }