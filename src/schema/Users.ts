import { objectType, stringArg, intArg, booleanArg, extendType } from '@nexus/schema';
import { IContext } from '../types/context';
import bcrypt from 'bcrypt';
import { AuthenticationError } from 'apollo-server-express';
const LoginUser = objectType({
    name: "LoginUser",
    definition(t) {
        t.string("email")
        t.string("nick")
        t.int("role")
    }
})



export const User = objectType({
    name: "User",
    definition(t) {
        t.string("email", { nullable: true })
        t.int("id", { nullable: true, })
        t.boolean("isActive", { nullable: true })
        t.boolean("isOnExam", { nullable: true })
        t.string("password", { nullable: true })
        t.int("role", { nullable: true })
        t.string("nick")
        t.string("createAt", { nullable: true })
    }
})

export const UsersMutation = extendType({
    type: "Mutation",
    definition(t) {
        t.field("createUser", {
            type: User,
            args: {
                email: stringArg({ required: true }),
                password: stringArg({ required: true }),
                nick: stringArg({ required: true }),
            },

        })
        t.boolean("deleteUser", {
            args: {
                id: intArg({ required: true }),
            },
            async resolve(_root, args, { db }: IContext) {
                try {
                    await db.user.delete({
                        where: {
                            id: args.id
                        }
                    })
                    return true;
                } catch (error) {
                    throw new Error(error)
                }
            }

        })
        t.field("updateUser", {
            type: User,
            nullable: true,
            args: {
                id: intArg({ required: true }),
                email: stringArg(),
                isActive: booleanArg(),
                isOnExam: booleanArg(),
                password: stringArg(),
                role: intArg(),
                nick: stringArg(),
            },
            async resolve(_root, args, { db }: IContext) {
                type UserType = {
                    id: number,
                    email: string,
                    nick: string,
                    password: string,
                    role: number,
                    isActive: boolean,
                    isOnExam: boolean,
                };
                try {
                    const UpdatedUser = await db.user.update({
                        data: args as Omit<UserType, 'id'>,
                        where: {
                            id: args.id
                        }
                    })
                    return UpdatedUser;
                } catch (error) {
                    throw new Error(error)
                }
            }
        })
        t.field("loginUser", {
            type: LoginUser,
            args: {
                email: stringArg({ required: true }),
                password: stringArg({ required: true }),
            },
            async resolve(_parent, args, { req, db }: IContext) {
                const User = await db.user.findMany({
                    where: {
                        email: args.email
                    },
                    take: 1
                });
                if (User.length > 0) {
                    const correctPassword = await bcrypt.compare(args.password, User[0].password);
                    if (!correctPassword || User.length === 0) throw new AuthenticationError("Niepoprawny email lub hasło")
                    const { email, nick, role, id } = User[0];
                    req.session!.isLogin = true;
                    req.session!.email = email;
                    req.session!.nick = nick;
                    req.session!.role = role;
                    req.session!.userId = id.toString();
                    return { email, nick, role, id };
                } else {
                    throw new AuthenticationError("Niepoprawny email lub hasło")
                }

            }
        })
        t.boolean("logoutUser", {
            resolve(_root, _args, { req }: IContext) {
                if (req.session!.isLogin) {
                    req.session!.destroy((err) => err);
                    return true;
                }
                return false;
            }
        })
    }
});

export const UsersQuery = extendType({
    type: "Query",
    definition(t) {
        t.field("me", {
            type: LoginUser,
            resolve(_root, _args, { req }: IContext) {
                return {
                    email: req.session!.email,
                    nick: req.session!.nick,
                    role: req.session!.role,
                }
            }
        })
        t.field("getUserById", {
            type: User,
            args: {
                id: intArg({ required: true }),
            },
            async resolve(_root, args, { db }: IContext) {
                const User = await db.user.findOne({
                    where: {
                        id: args.id
                    }
                })
                return User;
            }
        })
        t.list.field("getAllUsers", {
            type: User,
            args: {
                take: intArg({ required: true }),
                skip: intArg({ required: true }),
            },
            async resolve(_root, args, { db }: IContext) {
                const Users = await db.user.findMany({
                    take: args.take,
                    skip: args.skip,
                })
                return Users;
            }
        })
    }
});
