import { objectType, stringArg, booleanArg, intArg, mutationType, queryType, extendType } from '@nexus/schema';
import { Questions } from './Questions';

export const Answer = objectType({
    name: "Answer",
    definition(t) {
        t.int("id")
        t.string("question")
        t.int("questionId")
        t.string("answer")
        t.string("answeredAt")
        t.int("examId")
        t.boolean("isCorrectAnswer", { nullable: true })
        t.string("examerNote", { nullable: true })
        t.boolean("isRated", { nullable: true })
        t.int("getScored", { nullable: true })
        t.field("Questions", { type: Questions })
    }
});

export const AnswersMutation = extendType({
    type: "Mutation",
    definition(t) {
        t.boolean("answerQuestion", {
            args: {
                answer: stringArg(),
            },
        })
        t.field("rateTheAnswer", {
            type: Answer,
            args: {
                isCorrectAnswer: booleanArg({ required: true }),
                getScored: intArg({ required: true }),
                answerId: intArg({ required: true }),
                examerNote: stringArg(),
            },
        })
    }
});

export const AnswersQuery = extendType({
    type: "Query",
    definition(t) {
        t.list.field("getAnswersByExamId", {
            type: Answer,
            args: {
                examId: intArg({ required: true }),
                isRated: booleanArg(),
                take: intArg(),
                skip: intArg(),
            },
        })
    }
});