import { PrismaClient, PrismaClientOptions } from '@prisma/client'

type ValidationErrorType = {
    param: string,
    msg: string,
}
const prisma = new PrismaClient({
    log: ['query', 'info', 'warn'],
  });
   
export interface IContext {
    res?: Express.Response,
    req: Express.Request,
    db: PrismaClient<PrismaClientOptions, never>
    validationErrors?: ValidationErrorType[]
}

export const createContext = async ({ req, res }: Pick<IContext, 'req' |'res'>) => {
    return {
      req,
      res,
      db: prisma,
    };
}