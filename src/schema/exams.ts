import { objectType, stringArg, intArg, extendType } from '@nexus/schema';
import { Answer } from './Answers';
import { User } from './Users';

export const Exams = objectType({
    name: "Exams",
    definition(t) {
        t.int("id", { nullable: true })
        t.string("activeCode")
        t.string("examerNick")
        t.string("examSubject")
        t.int("examinatedUserId")
        t.list.int("genQuestionsId")
        t.string("startDate", { nullable: true })
        t.string("endDate", { nullable: true })
        t.string("examDetails", { nullable: true })
        t.string("createAt", { nullable: true })
        t.boolean("isPassed", { nullable: true })
        t.boolean("isActive", { nullable: true })
        t.int("score", { nullable: true })
        t.int("minScoreToPass", { nullable: true })
        t.field("User", {
            type: User,
            nullable: true,
        })
        t.list.field("Answers", {
            type: Answer,
            nullable: true,
        })
    }
})

export const ExamsMutation = extendType({
    type: "Mutation",
    definition(t) {
        t.list.field("assignToExam", {
            type: Exams,
            args: {
                activeCode: stringArg({ required: true }),
                examId: intArg({ required: true }),
            },
        })
        t.field("addExam", {
            type: Exams,
            args: {
                examSubject: stringArg({ required: true }),
                examinatedUserId: intArg({ required: true }),
                questionIds: intArg({
                    list: true,
                    required: true
                }),
                minScoreToPass: intArg({ required: true }),
                startDate: stringArg(),
                endDate: stringArg(),
                examDetails: stringArg(),
            },
        })
        t.boolean("deleteExam", {
            args: {
                id: intArg({ required: true }),
            },
        })
        t.string("finishExam")
        t.boolean("finishExamByUserId", {
            args: {
                id: intArg({ required: true }),
            },
        })
    }
})
export const ExamsQuery = extendType({
    type: "Query",
    definition(t) {
        t.list.field("getAllExams", {
            type: Exams,
            args: {
                take: intArg({ required: true }),
                skip: intArg({ required: true }),
            },
        })
        t.list.field("getExamsByLoginUser", {
            type: Exams,
            args: {
                take: intArg({ required: true }),
                skip: intArg({ required: true }),
            },
        })
        t.list.field("getExamsByUserId", {
            type: Exams,
            args: {
                userId: intArg({ required: true }),
            },
        })
        t.field("getExamById", {
            type: Exams,
            nullable: true,
            args: {
                examId: intArg({ required: true }),
            },
        })
    }
})
