import { objectType, intArg, stringArg, extendType } from "@nexus/schema";

export const Questions = objectType({
    name: "Questions",
    definition(t) {
        t.int("id", { nullable: true })
        t.string("content")
        t.string("type")
        t.int("score")
        t.string("odpA", { nullable: true })
        t.string("odpB", { nullable: true })
        t.string("odpC", { nullable: true })
        t.string("odpD", { nullable: true })
        t.string("correctAnswer", { nullable: true })
        t.string("image", { nullable: true })
    }
});

export const QuestionsMutation = extendType({
    type: "Mutation",
    definition(t) {
        t.list.int("getRandomQuestion", {
            args: {
                questionsToGen: intArg({ required: true }),
            },
        })
        t.field("addQuestion", {
            type: Questions,
            args: {
                content: stringArg({ required: true }),
                type: stringArg({ required: true }),
                score: intArg({ required: true }),
                odpA: stringArg(),
                odpB: stringArg(),
                odpC: stringArg(),
                odpD: stringArg(),
                correctAnswer: stringArg(),
                image: stringArg(),
            },
        })
        t.boolean("deleteQuestion", {
            args: {
                id: intArg({ required: true }),
            },
        })
        t.field("updateQuestion", {
            type: Questions,
            args: {
                id: intArg({ required: true }),
                content: stringArg(),
                type: stringArg(),
                score: intArg(),
                odpA: stringArg(),
                odpB: stringArg(),
                odpC: stringArg(),
                odpD: stringArg(),
                correctAnswer: stringArg(),
                image: stringArg(),
            },
        })
        t.field("getNextQuestion", { type: Questions })
    }
})
export const QuestionsQuery = extendType({
    type: "Query",
    definition(t) {
        t.field("getQuestionById", {
            type: Questions,
            args: {
                id: intArg({ required: true }),
            },
        })
        t.list.field("getAllQuestions", {
            type: Questions,
            args: {
                take: intArg({ required: true }),
                skip: intArg({ required: true }),
            },
        })
    }
})