import { makeSchema } from '@nexus/schema'
import { join } from 'path'
import * as Alltypes from './types';

export const schema = makeSchema({
  types: Alltypes,
  outputs: {
    typegen: join(__dirname, '..', './generated/nexus-typegen.ts'),
    schema: join(__dirname, '..', './generated/schema.graphql')
  },
  typegenAutoConfig: {
    contextType: 'Context.Context',
    sources: [
      {
        source: '@prisma/client',
        alias: 'prisma',
      },
      {
        source: require.resolve('./context'),
        alias: 'Context',
      },
    ],
  },
})

