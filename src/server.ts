import { ApolloServer } from 'apollo-server-express';
import session from "express-session";
import express from 'express';
import { schema } from './schema/schema';
import dotenv from 'dotenv';
import helmet from 'helmet';
import { PrismaClient } from '@prisma/client'
import rateLimit from 'express-rate-limit';
import { IContext } from './types/context';
import depthLimit from 'graphql-depth-limit'
import { createContext } from './schema/context';


const DepthLimitRule = depthLimit(
  4,
  { ignore: ['whatever', 'trusted'] },
)
dotenv.config();
const server = new ApolloServer({
  schema,
  tracing: true,
  validationRules: [
    DepthLimitRule,
  ],
  context: async ({ req, res }: IContext) => {
    return createContext({req, res});
  },
  playground: {
    settings: { 'request.credentials': 'include', 'editor.cursorShape': 'line' } as any
  },
});
const app = express();
app.use(rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100, // limit each IP to 100 requests per windowMs
}));
app.use(helmet());
app.use(session({
  name: 'sid',
  secret: process.env.SESSION_SECRET || 'exampleSecret',
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 7, //7Days
    httpOnly: true,
    sameSite: true,
    secure: process.env.NODE_ENV === "production"
  }
}));

server.applyMiddleware({
  app,
  cors: {
    methods: ["POST", "GET"],
    credentials: true,
    origin: `${process.env.GRAPHQL_SERVER_ORIGIN}`,
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"]
  }
});

app.listen({ port: Number(process.env.GRAPHQL_SERVER_PORT) }, async () => {
  console.log(`Server ready at ${process.env.GRAPHQL_SERVER_ORIGIN}:${process.env.GRAPHQL_SERVER_PORT}`);
});
