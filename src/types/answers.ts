export type AnswerQuestion = {
    answer: string,
}
export type AnswersByExamId = {
    examId: number,
    isRated: boolean,
}
export type AnswerById = {
    id: number,
}
export type RateTheAnswer = {
    answerId: number,
    isCorrectAnswer: boolean,
    examerNote?: string,
    getScored: number,
}