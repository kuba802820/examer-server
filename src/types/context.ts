import { PrismaClient, PrismaClientOptions } from '@prisma/client'

type ValidationErrorType = {
    param: string,
    msg: string,
}

export interface IContext {
    res?: Express.Response,
    req: Express.Request,
    db: PrismaClient<PrismaClientOptions, never>
    validationErrors?: ValidationErrorType[]
}