export type AddExam = {
    examSubject: string,
    startDate: number,
    endDate: number,
    examDetails: string,
    examinatedUserId: number
    questionIds: number[],
    minScoreToPass: number,
}
export type UserExamId = {
    userId: number,
}
export type AssignToExam = {
    activeCode: string,
    examId: number,
}