type SingleQuestion = 'Open' | 'YesNo' | "Close";

export type GenRandomQuestion = {
    questionsToGen: number,
}
export type QuestionId = {
    id: number,
}

export type Question = {
    content: string,
    score: number,
    type: SingleQuestion,
    correctAnswer: string,
    odpA: string,
    odpB: string,
    odpC: string,
    odpD: string,
    image: string,
}

