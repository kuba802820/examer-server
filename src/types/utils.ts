type Pagination = {
    skip: number,
    take: number,
}
type Filtering = {
    filter: string,
    fieldsToFilter: string[],
}
export interface PaginationAndFiltering extends Pagination, Filtering { };